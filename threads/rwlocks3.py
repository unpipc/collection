#!/usr/bin/env python

# Own implementation of reader-writer lock.

import threading
import logging

L = logging.getLogger(__name__)
L.setLevel(logging.DEBUG)
#L.setLevel(logging.INFO)
L.addHandler(logging.StreamHandler())

def dbg(msg):
    L.debug(msg)

def info(msg):
    L.info(msg)

def with_thread(func):
    def func_with_thread_arg(self, *args, **kwargs):
        t = threading.current_thread()

        return func(self, t, *args, **kwargs)
    
    return func_with_thread_arg


class RWLockException(Exception):
    pass

class RWLock(object):
    def __init__(self):
        # Mutex to secure access to the controlling
        # data below and for write-lock.
        #
        # If a thread has acquired this mutex, it
        # may implicitly read/modify the controlling data
        # through the acquire/release methods below.
        #
        # As well as that, this mutex is locked for the
        # writer until it releases it after writing
        # is complete.
        #
        # Readers do not hold the mutex locked during
        # reading operation, so other threads acquiring lock
        # during someone's reading may get this mutex 
        # for a while to be queued or get immediate 
        # access for read-only access.
        self.__mutex = threading.Lock()

        # Condition to notify pending writers for 
        # them to compete for write-access.
        self.__writer_cond = threading.Condition(self.__mutex)

        # Condition to notify pending readers for 
        # them to compete for read-only access.
        self.__reader_cond = threading.Condition(self.__mutex)

        # Set of pending writers. We need to count
        # writers wishing to have write-access in order
        # to decide whether a reader in attempt to acquire the
        # lock can have precedence in accessing the resource.
        #
        # If the set is not empty, every reader in attempt for
        # acquiring the lock will be queued.
        self.__pending_writers = set()

        # Writer who has succeded to acquire the write-lock.
        self.__active_writer = None

        # Readers who have successfully acquired read-only
        # lock. No writer will be granted write-access until
        # this set is empty.
        self.__active_readers = set()

    @with_thread
    def wr_acquire(self, t):
        self.__mutex.acquire()

        # The resource is free for access.
        # We need to decide now whether we can
        # grant access right now or put the thread
        # in the queue.

        # There is no need to check whether a writer
        # has acquired the lock for writing, cause it would 
        # have already had this lock acquired.
        #
        # Since this thread has successfully acquired it, no other
        # writing thread is using it. Though, some readers 
        # may be reading, cause readers do not hold the mutex
        # locked during reading operation.
        #while self.__active_writer or self.__active_readers:
        while self.__active_readers:
            self.__pending_writers.add(t)

            # Wait until all the readers
            # complete reading.
            self.__writer_cond.wait()

        # Writer has been awakened after the wait()
        # call and acquired the mutex.
        #
        # Alternatively, no active readers existed
        # and there was no need to call wait().

        # Use 'discard()' in case if the thread 
        # was not put in the set of pending writers.
        self.__pending_writers.discard(t)

        self.__active_writer = t

        # This thread has succeeded to get the write-lock.
        # Other threads will neither acquire any kind of lock,
        # nor they will be registered as pending until this 
        # thread releases the lock.

    @with_thread
    def rd_acquire(self, t):
        self.__mutex.acquire()

        # No need to check whether there is an active writer
        # now, cause we wouldn't have acquired the lock in
        # that case.
        #if self.__pending_writers or self.__active_writer:
        if self.__pending_writers:
            self.__reader_cond.wait()

        # No pending writers, no reason to discard read-only access.

        # Register this reader to discard any write-access
        # until it completes reading.
        self.__active_readers.add(t)

        # Release the lock to let coming writers
        # get registered as pending to be notified
        # after release.
        self.__mutex.release()

    @with_thread
    def release(self, t):
        # Do not attempt to lock the mutex to
        # avoid dead-lock (this thread might be
        # a writing thread having the lock.

        if t in self.__active_readers:
            # This thread is a reader.
            # Readers do not actually hold the mutex locked
            # during reading, so it is safe to acquire it now.
            self.__mutex.acquire()

            self.__active_readers.remove(t)
        elif t == self.__active_writer:
            # This thread is already oweing the lock.

            self.__active_writer = None
        else:
            raise RWLockException("Attempt to release unlocked lock.")

        if not self.__active_readers:
            if self.__pending_writers:
                self.__writer_cond.notify()
            else:
                self.__reader_cond.notify()

        self.__mutex.release()


