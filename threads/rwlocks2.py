#!/usr/bin/env python

# Own implementation of reader-writer lock.

import threading
import logging

L = logging.getLogger(__name__)
L.setLevel(logging.DEBUG)
#L.setLevel(logging.INFO)
L.addHandler(logging.StreamHandler())

def dbg(msg):
    L.debug(msg)

def info(msg):
    L.info(msg)

def with_thread(func):
    def func_with_thread_arg(self, *args, **kwargs):
        t = threading.current_thread()

        return func(self, t, *args, **kwargs)
    
    return func_with_thread_arg


class RWLockException(Exception):
    pass

class RWLock(object):
    def __init__(self):
        # Mutex to have access to the controlling
        # data below and for writing locks.
        # If a thread has acquired this mutex, it
        # may implicitly read/modify the controlling data
        # through the acquire/release methods below.
        # As well as that, this mutex is locked for the
        # writer until it releases it.
        # Readers do not hold the mutex locked during
        # reading operation, so other threads acquiring lock
        # may get this mutex for a while to be queued.
        self.mutex = threading.Lock()

        # Condition to notify pending writers for 
        # them to compete for write-access.
        self.writer_cond = threading.Condition(self.mutex)

        # Condition to notify pending readers for 
        # them to compete for read-only access.
        self.reader_cond = threading.Condition(self.mutex)

        # Set of pending writers. We need to count
        # writers wishing to have write-access in order
        # to decide whether a reader in attempt to acquire the
        # lock can have precedence in accessing the resource.
        # If the set is not empty, every reader in attempt for
        # acquiring the lock will be queued.
        self.pending_writers = set()

        # Writer who have succeded to acquire the write-lock.
        self.active_writer = None

        # Readers who have successfully acquired read-only
        # lock. No writer will be granted write-access until
        # this set is empty.
        self.active_readers = set()

    @with_thread
    def wr_acquire(self, t):
        self.mutex.acquire()

        # The resource is free for access.
        # We need to decide now whether we can
        # grant access right now or put the thread
        # in the queue.

        # There is not need to check whether a writer
        # has acquired the lock for writing, cause it would 
        # have already had this lock acquired.
        # Since this thread has successfully acquired it, no other
        # writing thread is using it. Though, some readers 
        # may be reading, cause readers do not hold the mutex
        # locked during reading operation.
        #while self.active_writer or self.active_readers:
        while self.active_readers:
            self.pending_writers.add(t)

            # Wait until all the readers
            # complete reading.
            self.writer_cond.wait()

        # Writer has been awakened after the wait()
        # call and acquired the mutex.
        # Alternatively, no active readers existed
        # and there was no need to call wait()

        # Use 'discard()' in case if the thread 
        # is not in the set.
        self.pending_writers.discard(t)

        self.active_writer = t

        # This thread has succeeded to get the write-lock.
        # Other threads will neither acquire any kind of lock,
        # nor they will be queued until this thread releases
        # the lock.

    @with_thread
    def rd_acquire(self, t):
        self.mutex.acquire()

        # No need to check whether there is an active writer
        # now, cause we wouldn't have acquired the lock in
        # that case.
        #if self.pending_writers or self.active_writer:
        if self.pending_writers:
            self.reader_cond.wait()

        # No pending writers, no reason to discard read-only access.

        # Register this reader to discard any write-access
        # until it completes reading.
        self.active_readers.add(t)

        self.mutex.release()

    @with_thread
    def release(self, t):
        # Do not attempt to lock the mutex to
        # avoid dead-lock (this thread might be
        # a writing thread having the lock.

        if t in self.active_readers:
            # This thread is a reader.
            # Readers do not actually hold the mutex locked
            # during reading, so it is safe to acquire it now.
            self.mutex.acquire()

            self.active_readers.remove(t)
        elif t == self.active_writer:
            # This thread is already having the lock.

            self.active_writer = None
        else:
            raise RWLockException("Attempt to release unlocked lock.")

        if not self.active_readers:
            if self.pending_writers:
                self.writer_cond.notify()
            else:
                self.reader_cond.notify()

        self.mutex.release()


