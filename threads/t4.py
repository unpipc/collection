#!/usr/bin/env python

import logging
import threading as thr
import time

L = logging.getLogger(__name__)
#L.setLevel(logging.DEBUG)
L.setLevel(logging.INFO)
L.addHandler(logging.StreamHandler())

def dbg(msg):
    L.debug(msg)

def info(msg):
    L.info(msg)

class Actor(object):
    def __init__(self, nthreads):
        self.nthreads = nthreads
        self.threads = list()
        self.thread_reports = dict()

    def act(self):
        for i in range(0, self.nthreads):
            self.threads.append(thr.Thread(target=self.do, name="%d" % i))

        for t in self.threads:
            t.start()

    def join(self):
        cls = type(self).__name__

        dbg("join of %s" % cls)

        for t in self.threads:
            t.join()

        dbg("join of %s done" % cls)

    def get_reports(self):
        return [(key, self.thread_reports[key]) for key in sorted(self.thread_reports.keys())]


class Producer(Actor):
    def __init__(self, **kwargs):
        nthreads = kwargs.get('nthreads', 10)
        super(Producer, self).__init__(nthreads)

        self.products = list()
        self.max_products = kwargs.get('max_products', 100)

        self.mutex = thr.Lock()

        self.consumer = None
        self.sleep = kwargs.get('sleep')

    def set_consumer(self, consumer):
        self.consumer = consumer

    def lock(self, t):
        dbg('Producer %s: acquiring lock()...' % t)
        self.mutex.acquire()
        dbg('Producer %s: got it.' % t)

    def unlock(self, t):
        self.mutex.release()
        dbg('Producer %s: lock released.' % t)

    def do(self):
        t_cur = thr.current_thread().getName()
        self.thread_reports[t_cur] = 0

        done = False

        while not done:
            self.lock(t_cur)

            l = len(self.products)

            if l < self.max_products:
                self.products.append(l)
                self.unlock(t_cur)

                dbg('Producer %s produced: %d' % (t_cur, l))

                self.thread_reports[t_cur] += 1

                # Emulate load:
                if self.sleep is not None:
                    time.sleep(self.sleep)
            else:
                done = True
                self.unlock(t_cur)

                # Although, all the products have been produced,
                # we must go on to notify consumer.

            if self.consumer and not done:
                self.consumer.lock("p%s" % t_cur)

                if self.consumer.nready == 0:
                    # Notify blocked consumers.
                    self.consumer.notify_all()

                # Flag an awakened consumer that it 
                # might consume one more product.
                self.consumer.nready += 1
                dbg('Producer %s flagged nready: %d' % (t_cur, self.consumer.nready))

                self.consumer.unlock("p%s" % t_cur)
        
class Consumer(Actor):
    def __init__(self, producer, **kwargs):
        nthreads = kwargs.get('nthreads', 10)
        super(Consumer, self).__init__(nthreads)

        self.producer = producer

        self.mutex = thr.Lock()
        self.cv = thr.Condition(self.mutex)

        # Amount of new products ready for consuming.
        self.nready = 0

        # How many products have been consumed so far.
        self.nconsumed = 0

        # For load emulation.
        self.sleep = kwargs.get('sleep')

    def lock(self, t):
        dbg('Consumer %s: acquiring lock()...' % t)
        self.mutex.acquire()
        dbg('Consumer %s: got it.' % t)

    def unlock(self, t):
        self.mutex.release()
        dbg('Consumer %s: lock released.' % t)

    def wait(self, t):
        dbg('Consumer %s: waiting cv.' % t)
        self.cv.wait()
        dbg('Consumer %s: got cv.' % t)

    def notify(self):
        self.cv.notify()
        dbg('Notified')

    def notify_all(self):
        self.cv.notifyAll()
        dbg('Notified all')

    def do(self):
        t_cur = thr.current_thread().getName()

        self.thread_reports[t_cur] = [0, 0] # [num_correct, num_wrong]

        done = False

        while not done:
            self.lock(t_cur)

            while not done and self.nready == 0:
                if self.nconsumed == self.producer.max_products:
                    done = True
                else:
                    # Wait for new product or production stop flag.
                    dbg('Consumer %s is waiting for nready != 0 (%d, %d/%d)' % 
                            (t_cur, self.nready, self.nconsumed, self.producer.max_products))
                    self.wait(t_cur)

            if not done:
                # 'Consume' one product.
                if self.producer.products[self.nconsumed] == self.nconsumed:
                    self.thread_reports[t_cur][0] += 1
                else:
                    self.thread_reports[t_cur][1] += 1

                self.nconsumed += 1
                self.nready -= 1
                # nready is 0 as maximum after decrementing.

                dbg('Consumer %s has consumed 1 product (%d)' % (t_cur, self.nready))

            self.unlock(t_cur)

            if not done:
                # Emulate load:
                if self.sleep is not None:
                    time.sleep(self.sleep)

for nprod in (10000,):
    info("Number of products: %d" % nprod)

    p = Producer(max_products=nprod, nthreads=10, sleep=0.001)
    #p = Producer(max_products=nprod, nthreads=10)
    c = Consumer(p, nthreads=10)
    p.set_consumer(c)

    t1 = time.time()
    p.act()
    c.act()

    p.join()
    c.join()
    t2 = time.time()

    d = t2 - t1
    info('Duration: %.3f sec' % d)
    info('Duration/product: %.3f ms\n' % (d/nprod * 1000))

    info('Producers:')

    total_count = 0
    for t, count in p.get_reports():
        info("%s: %d" % (t, count))
        total_count += count

    info("Total produced: %d\n" % total_count)

    info('Consumers:')

    total_num_correct = 0
    total_num_wrong = 0
    for t, report in c.get_reports():
        num_correct, num_wrong = report
        total_num_correct += num_correct
        total_num_wrong += num_wrong
        info("%s: %d, %d" % (t, num_correct, num_wrong))

    info("Total correct values: %d" % total_num_correct)
    info("Total wrong values: %d\n" % total_num_wrong)
