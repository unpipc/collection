#!/usr/bin/env python

import threading
import rwlocks3 as rwlocks

import logging
import time

L = logging.getLogger(__name__)
#L.setLevel(logging.DEBUG)
L.setLevel(logging.INFO)
L.addHandler(logging.StreamHandler())

def dbg(msg):
    L.debug(msg)

def info(msg):
    L.info(msg)

class Actor(object):
    def __init__(self, nthreads):
        self.nthreads = nthreads
        self.threads = list()
        self.thread_reports = dict()

    def act(self):
        for i in range(0, self.nthreads):
            self.threads.append(threading.Thread(target=self.do, name="%d" % i))

        for t in self.threads:
            t.start()

    def join(self):
        cls = type(self).__name__

        dbg("join of %s" % cls)

        for t in self.threads:
            t.join()

        dbg("join of %s done" % cls)

    def get_reports(self):
        return [(key, self.thread_reports[key]) for key in sorted(self.thread_reports.keys())]


class Writer(Actor):
    def __init__(self, **kwargs):
        nthreads = kwargs.get('nthreads', 10)
        super(Writer, self).__init__(nthreads)

        self.count = 0
        self.max_count = kwargs.get('max_count', 100)
        buf_len = kwargs.get('buf_len', 100)
        self.buf = [0] * buf_len
        self.sleep = kwargs.get('sleep')

        self.mutex = rwlocks.RWLock()

    def lock(self, t):
        dbg('Writer %s: acquiring lock()...' % t)
        self.mutex.wr_acquire()
        dbg('Writer %s: got lock.' % t)

    def unlock(self, t):
        dbg('Writer %s is going to release lock.' % t)
        self.mutex.release()
        dbg('Writer %s has released lock.' % t)

    def do(self):
        t_cur = threading.current_thread().getName()
        self.thread_reports[t_cur] = 0

        done = False

        while not done:
            self.lock(t_cur)

            if self.count < self.max_count:
                self.count += 1
                for i, v in enumerate(self.buf):
                    self.buf[i] = self.count

                    if self.sleep is not None and (i+1)/5 == 0:
                        time.sleep(self.sleep)
        
                self.thread_reports[t_cur] += 1
            else:
                done = True

            self.unlock(t_cur)
class Reader(Actor):
    def __init__(self, writer, **kwargs):
        nthreads = kwargs.get('nthreads', 10)
        super(Reader, self).__init__(nthreads)

        self.writer = writer
        self.done = False

        # For load emulation.
        self.sleep = kwargs.get('sleep')

    def lock(self, t):
        dbg('Reader %s: acquiring lock()...' % t)
        self.writer.mutex.rd_acquire()
        dbg('Reader %s: got lock.' % t)

    def unlock(self, t):
        self.writer.mutex.release()
        dbg('Reader %s: has released lock.' % t)

    def shutdown(self):
        self.done = True

    def do(self):
        t_cur = threading.current_thread().getName()

        self.thread_reports[t_cur] = [0, 0] # [num_correct, num_wrong]

        while not self.done:
            # Readers cannot set counters because lock for writing
            # would be needed for that.

            self.lock(t_cur)

            # Reader will stop checking buffers when it
            # is flagged by self.done=True

            if self.writer.buf:
                x = self.writer.buf[0]
                correct = True

                for n in self.writer.buf[1:]:
                    if n != x:
                        correct = False
                        break

                if correct:
                    self.thread_reports[t_cur][0] += 1
                else:
                    self.thread_reports[t_cur][1] += 1

            if self.writer.buf:
                dbg('Reader %s has checked 1 buffer [%d,...]' % (t_cur, x))

            self.unlock(t_cur)

            if self.sleep is not None:
                time.sleep(self.sleep)


w = Writer(max_count=1000, buf_len=10000, nthreads=10)
r = Reader(w, nthreads=5)

t1 = time.time()
w.act()
r.act()

w.join()
r.shutdown()
r.join()
t2 = time.time()

d = t2 - t1
info('Duration: %.3f sec' % d)

info('Writers:')

total_count = 0
for t, count in w.get_reports():
    info("%s: %d" % (t, count))
    total_count += count

info("Total written: %d buffer(s)\n" % total_count)

info('Readers:')

total_num_correct = 0
total_num_wrong = 0
for t, report in r.get_reports():
    num_correct, num_wrong = report
    total_num_correct += num_correct
    total_num_wrong += num_wrong
    info("%s: %d, %d" % (t, num_correct, num_wrong))

info("Total successful checks: %d" % total_num_correct)
info("Total failed checks: %d\n" % total_num_wrong)
