#!/usr/bin/env python

import threading as thr

class Actor(object):
    def __init__(self, nthreads):
        self.nthreads = nthreads
        self.threads = list()

    def act(self):
        for i in range(0, self.nthreads):
            self.threads.append(thr.Thread(target=self.do, name="%d" % i))

        for t in self.threads:
            t.start()

    def join(self):
        for t in self.threads:
            t.join()

class Producer(Actor):
    def __init__(self, **kwargs):
        nthreads = kwargs.get('nthreads', 10)
        super(Producer, self).__init__(nthreads)

        self.products = list()
        self.max_products = kwargs.get('max_products', 100)

        self.thread_reports = dict()
        self.mutex = thr.Lock()

        self.consumer = None

    def lock(self):
        print 'Producer: acquiring lock()...'
        self.mutex.acquire()
        print 'Producer: got it.'

    def unlock(self):
        self.mutex.release()
        print 'Producer: lock released.'

    def get_reports(self):
        return sorted(zip(self.thread_reports.keys(), self.thread_reports.values()), lambda t1, t2: cmp(int(t1[0].getName()), int(t2[0].getName())))

    def do(self):
        t_cur = thr.current_thread()
        self.thread_reports[t_cur] = 0

        while True:
            self.lock()

            l = len(self.products)

            if l == self.max_products:
                break
            elif l > 0:
                self.products.append(self.products[-1] + 1)
            else:
                self.products.append(0)

            self.thread_reports[t_cur] += 1
            self.unlock()

        self.unlock()
        
class Consumer(Actor):
    def __init__(self, producer, **kwargs):
        nthreads = kwargs.get('nthreads', 10)
        super(Consumer, self).__init__(nthreads)

        self.producer = producer

        self.mutex = thr.Lock()
        self.thread_reports = dict()
        self.nconsumed = 0

    def lock(self):
        print 'Consumer: acquiring lock()...'
        self.mutex.acquire()
        print 'Consumer: got it.'

    def unlock(self):
        self.mutex.release()
        print 'Consumer: lock released.'

    def do(self):
        t_cur = thr.current_thread()

        self.thread_reports[t_cur] = [0, 0] # [num_correct, num_wrong]

        while True:
            self.lock()

            if self.nconsumed == self.producer.max_products:
                break

            while True:
                self.producer.lock()
                nproducts = len(self.producer.products)
                self.producer.unlock()

                if nproducts > self.nconsumed:
                    break

            if self.producer.products[self.nconsumed] == self.nconsumed:
                self.thread_reports[t_cur][0] += 1
            else:
                self.thread_reports[t_cur][1] += 1

            self.nconsumed += 1
            self.unlock()
            
        self.unlock()

    def get_reports(self):
        return sorted(zip(self.thread_reports.keys(), self.thread_reports.values()), lambda t1, t2: cmp(int(t1[0].getName()), int(t2[0].getName())))


for m in (1000,):
    print "Number of products is %d" % m

    p = Producer(max_products=m, nthreads=10)
    c = Consumer(p, nthreads=3)

    p.act()
    c.act()

    p.join()
    c.join()

    total_count = 0
    for t, count in p.get_reports():
        print "%s: %d" % (t.getName(), count)
        total_count += count

    print "Total produced: %d" % total_count
    print

    total_num_correct = 0
    total_num_wrong = 0
    for t, report in c.get_reports():
        num_correct, num_wrong = report
        total_num_correct += num_correct
        total_num_wrong += num_wrong
        print "%s: %d, %d" % (t.getName(), num_correct, num_wrong)

    print "Total correct values: %d" % total_num_correct
    print "Total wrong values: %d" % total_num_wrong
    print
