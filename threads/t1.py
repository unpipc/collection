#!/usr/bin/env python

import threading as thr

class Actor(object):
    def __init__(self, nthreads):
        self.nthreads = nthreads
        self.threads = list()

    def act(self):
        for i in range(0, self.nthreads):
            self.threads.append(thr.Thread(target=self.do, name="%d" % i))

        for t in self.threads:
            t.start()

    def join(self):
        for t in self.threads:
            t.join()

class Producer(Actor):
    def __init__(self, **kwargs):
        nthreads = kwargs.get('nthreads', 10)
        super(Producer, self).__init__(nthreads)

        self.products = list()
        self.max_products = kwargs.get('max_products', 100)

        self.thread_reports = dict()
        self.mutex = thr.Lock()

    def lock(self):
        self.mutex.acquire()

    def unlock(self):
        self.mutex.release()

    def get_reports(self):
        return sorted(zip(self.thread_reports.keys(), self.thread_reports.values()), lambda t1, t2: cmp(int(t1[0].getName()), int(t2[0].getName())))

    def do(self):
        t_cur = thr.current_thread()
        self.thread_reports[t_cur] = 0
        result = True

        while result:
            self.lock()

            result = self.produce(t_cur)

            self.unlock()

    def produce(self, thread):
        l = len(self.products)

        if l == self.max_products:
            return False
        elif l > 0:
            self.products.append(self.products[-1] + 1)
        else:
            self.products = [0]

        self.thread_reports[thread] += 1

        return True
        
class Consumer(Actor):
    def __init__(self, producer, **kwargs):
        nthreads = kwargs.get('nthreads', 10)
        super(Consumer, self).__init__(nthreads)

        self.producer = producer
        self.thread_reports = dict()
        self.nconsumed = 0

    def do(self):
        t_cur = thr.current_thread()

        self.thread_reports[t_cur] = [0, 0] # [num_correct, num_wrong]
        result = True

        while result:
            self.producer.lock()
            result = self.consume(t_cur)
            self.producer.unlock()

    def consume(self, thread):
        nproduced = len(self.producer.products)

        if self.nconsumed == self.producer.max_products:
            return False
        elif nproduced > self.nconsumed:
            if self.producer.products[self.nconsumed] == self.nconsumed:
                self.thread_reports[thread][0] += 1
            else:
                self.thread_reports[thread][1] += 1

            self.nconsumed += 1

            return True

    def get_reports(self):
        return sorted(zip(self.thread_reports.keys(), self.thread_reports.values()), lambda t1, t2: cmp(int(t1[0].getName()), int(t2[0].getName())))


for m in (10, 100, 1000, 10000):
    print "Number of products is %d" % m

    p = Producer(max_products=m, nthreads=20)
    c = Consumer(p)

    p.act()
    c.act()

    p.join()
    c.join()

    total_count = 0
    for t, count in p.get_reports():
        print "%s: %d" % (t.getName(), count)
        total_count += count

    print "Total produced: %d" % total_count
    print

    total_num_correct = 0
    total_num_wrong = 0
    for t, report in c.get_reports():
        num_correct, num_wrong = report
        total_num_correct += num_correct
        total_num_wrong += num_wrong
        print "%s: %d, %d" % (t.getName(), num_correct, num_wrong)

    print "Total correct values: %d" % total_num_correct
    print "Total wrong values: %d" % total_num_wrong
    print
