class MultipletonException(Exception):
    pass

def multipleton(nmax_objects):
    class MultipletonMCMC(type):
        def __init__(cls, *args):
            if nmax_objects < 0:
                raise MultipletonException("Negative number of objects: %d" % nmax_objects)

            cls.objects = list()

            if nmax_objects == 0:
                pass

            if nmax_objects == 1:
                cls.get_object = lambda cls: cls.objects[0]

            if nmax_objects > 1:
                cls.get_objects = lambda cls: cls.objects

    class MultipletonMC(type):
        __metaclass__ = MultipletonMCMC

        def __init__(cls, *args):
            user_init = cls.__init__

            def init(self, *args, **kw):
                if len(cls.objects) == nmax_objects:
                    raise MultipletonException("Too many objects: %d." % nmax_objects)
                else:
                    cls.objects.append(self)

                user_init(self, *args, **kw)

            cls.__init__ = init

            super(MultipletonMC, cls).__init__(*args)

        def get_nmax_objects(cls):
            return nmax_objects

    return MultipletonMC

Singleton = multipleton(1)
