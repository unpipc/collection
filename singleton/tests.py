#!/usr/bin/env python

from singleton import *
import unittest

class S0(object):
    __metaclass__ = multipleton(0)

class S1(object):
    __metaclass__ = Singleton

    def __init__(self, value):
        self.value = value

class S2(object):
    __metaclass__ = multipleton(2)

    def __init__(self, value):
        self.value = value


class Test(unittest.TestCase):
    def test_s_1(self):
        self.assertRaises(MultipletonException, multipleton, -1)

    def test_s0(self):
        self.assertRaises(AttributeError, getattr, S0, 'get_objects')
        self.assertRaises(AttributeError, getattr, S0, 'get_object')
        self.assertRaises(MultipletonException, S0)

    def test_s1(self):
        self.assertRaises(AttributeError, getattr, S1, 'get_objects')
        self.assertNotEqual(S1.get_object, None)
        self.assertEqual(S1.get_nmax_objects(), 1)

        s1 = S1(1)
        self.assertEqual(s1.value, 1)

        self.assertRaises(AttributeError, getattr, s1, 'get_object')
        self.assertRaises(AttributeError, getattr, s1, 'get_objects')
        self.assertEqual(S1.get_object().value, 1)
        self.assertRaises(MultipletonException, S1)

    def test_s2(self):
        self.assertRaises(AttributeError, getattr, S2, 'get_object')
        self.assertEqual(S2.get_objects(), list())
        S2(0)
        S2(2)
        self.assertEqual(len(S2.get_objects()), 2)
        self.assertEqual(S2.get_objects()[0].value, 0)
        self.assertEqual(S2.get_objects()[1].value, 2)
        self.assertRaises(MultipletonException, S2, 0)



if __name__ == '__main__':
    unittest.main()

