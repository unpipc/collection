import random
from heapq import heapify, heappush, heappop, heappushpop, heapreplace
from operator import itemgetter
from itertools import dropwhile, islice
import timer
import unittest


def multiplex_list(*iterables):
    iters = tuple(map(iter, iterables))
    face = []

    for it in iters:
        try:
            face.append((next(it), it))
        except StopIteration:
            continue

    face.sort(key=itemgetter(0))

    while face:
        val, it = face.pop(0)
        yield val

        try:
            new_val = next(it)
        except StopIteration:
            continue

        try:
            slot = next(dropwhile(lambda x: x[1][0] < new_val,
                                  enumerate(face)))
            slot = slot[0]
        except StopIteration:
            slot = len(face)

        face.insert(slot, (new_val, it))


def multiplex_heapq(*iterables):
    iters = tuple(map(iter, iterables))
    face = []

    for no, it in enumerate(iters):
        try:
            # Put `no' as a tie-breaker, so two entries with
            # equal values will be returned in the order they were created.
            # https://docs.python.org/3.5/library/heapq.html#priority-queue-implementation-notes
            heappush(face, (next(it), no, it))
        except StopIteration:
            pass

    while face:
        val, no, it = face[0]

        try:
            val, no, it = heapreplace(face, (next(it), no, it))
        except StopIteration:
            heappop(face)

        yield val


def multiplex_heapq_repeated_values_optimized(*iterables):
    iters = tuple(map(iter, iterables))
    face = []

    for no, it in enumerate(iters):
        try:
            # Put `no' as a tie-breaker, so two entries with
            # equal values will be returned in the order they were created.
            # https://docs.python.org/3.5/library/heapq.html#priority-queue-implementation-notes
            heappush(face, (next(it), no, it))
        except StopIteration:
            pass

    while face:
        val, no, it = face[0]
        eq = False

        try:
            new_val = next(it)

            if new_val == val:
                yield new_val
                eq = True
            else:
                if eq:
                    eq = False
                    heappop(face)
                else:
                    val, no, it = heapreplace(face, (new_val, no, it))
                    yield val
        except StopIteration:
            heappop(face)
            yield val


#multiplex = multiplex_heapq
multiplex = multiplex_heapq_repeated_values_optimized


def gen(max_elements=1000, max_step=10):
    v = 0

    for _ in range(random.randint(0, max_elements)):
        v += random.randint(0, max_step)
        yield v


def gen2(max_elements=1000, max_step=10):
    v = 0

    for _ in range(max_elements):
        v += random.randint(0, max_step)
        yield v


class Test(unittest.TestCase):
    def test_void(self):
        self.assertEqual(list(multiplex()), [])
        self.assertEqual(list(multiplex((), [], range(0))), [])

    def test_basic(self):
        m = multiplex([1, 3, 3, 10, 15], [3, 5, 15, 25])
        self.assertEqual(list(m), [1, 3, 3, 3, 5, 10, 15, 15, 25])

    def test_infinite(self):
        gens = (gen(3*r, r) for r in (random.randint(0, 100) for _ in range(10)))
        multiplexed = list(islice(multiplex(*gens), 100))

        self.assertEqual(multiplexed, sorted(multiplexed))


if __name__ == '__main__':
    def report(fun, desc):
        t = timer.Timer()
        gens = list(gen2(100 * r, r) for r in range(10))
        gens.append((0 for _ in range(10000)))

        with t:
            list(fun(*gens))

        print('{}: {}μs'.format(desc, t.delta.microseconds))

    report(multiplex_heapq, 'Heap-list-based solution I')
    report(multiplex_heapq_repeated_values_optimized, 'Heap-list-based solution II')
    report(multiplex_list, 'List-based solution')
