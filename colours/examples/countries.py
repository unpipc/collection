from colours import colorise

countries = [
    ['Russia', 'Poland', 'Belarus', 'Ukraine'],
    ['Poland', 'Lituania'],
    ['Russia', 'Kazakhstan', 'China'],
    ['Russia', 'Mongolia', 'China'],
    ['Russia', 'Lituania', 'Latvia', 'Belarus'],
    ['Kirgizia', 'Kazakhstan', 'China'],
    ['Germany', 'Poland', 'Czech Republic']
]

inks = ['red', 'green', 'blue', 'yellow']
result = sorted(colorise(countries, inks),
                key=lambda item: inks.index(item[1]))

print('\n'.join(map(lambda item: '{:16s} {}'.format(*item), result)))

