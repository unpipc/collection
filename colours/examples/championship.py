from colours import colorise, championship
from itertools import groupby
from operator import itemgetter

key = itemgetter(1)
sched = championship(['Bears', 'Wolfs', 'Birds', 'Cats', 'Dogs'])

for day, matches in groupby(sorted(colorise(sched, list(range(1, 20,))), key=key),
                            key=key):
    announce = 'Day {}'.format(day)
    line = '-' * len(announce)

    print('\n'.join((line, announce, line)))
    print('\n'.join('{} - {}'.format(*match) for match, _ in matches))
    print()
