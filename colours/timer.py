from datetime import datetime


class Timer:
    def __init__(self):
        self.start = None
        self.end = None
        self.delta = None

    def __enter__(self):
        self.start = datetime.now()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.end = datetime.now()
        self.delta = self.end - self.start