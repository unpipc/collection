import unittest
from itertools import dropwhile, product, chain, islice, count
from random import sample, randint
from timer import Timer


class Node:
    def __init__(self, obj):
        self.obj = obj
        self.colour = None
        self.restricted_colours = set()

        # Ordered set would be a better choice:
        self.neighbours = set()

    def __str__(self):
        return str(self.obj)

    def add_neighbours(self, neighbours):
        self.neighbours |= neighbours
        self.neighbours.discard(self)

    def restrict_colour(self, colour):
        if self.colour is None:
            self.restricted_colours.add(colour)
        
    def set_colour(self, palette, recommended_palette=()):
        try:
            self.colour = next(dropwhile(lambda colour: colour in self.restricted_colours,
                                         chain(recommended_palette, palette)))
        except StopIteration:
            raise ValueError('cannot set colour')

        for n in self.neighbours:
            n.restrict_colour(self.colour)

        return self.colour


def colorise(graph, palette):
    nodes = dict()

    for linked in graph:
        for obj in linked:
            nodes.setdefault(obj, Node(obj))

        linked_nodes = set(nodes[obj] for obj in linked)
        for node in linked_nodes:
            node.add_neighbours(linked_nodes)

    class Frame:
        def __init__(self, node):
            self.node = node
            self.neighbours = None

    def uncoloured(nodes):
        return (n for n in nodes if n.colour is None)

    stack = []
    for node in uncoloured(nodes.values()):
        assert not stack
        stack.append(Frame(node))

        while stack:
            frame = stack[-1]
            top_node = frame.node

            if top_node.colour is None:
                # Parent node yet uncolored.
                # Children (neighbours) are pushed to the stack
                # already coloured.

                top_node.set_colour(palette)
                yield top_node.obj, top_node.colour

            if frame.neighbours is None:
                # Processing the node for the first time.
                # Identify uncoloured neighbours and set up
                # generator to iterate over the neighbours.
                neighbours = tuple(uncoloured(top_node.neighbours))
                frame.neighbours = (n for n in neighbours)

                for n in neighbours:
                    # Colour uncoloured neighbours.
                    n.set_colour(palette)
                    yield n.obj, n.colour

            try:
                # Push to the stack the next neighbour
                # that has been coloured just now.
                stack.append(Frame(next(frame.neighbours)))
            except StopIteration:
                # Every neighbour is coloured.
                stack.pop()


def championship(teams):
    indexes = dict(zip(teams, count(0)))

    prod = map(lambda teams: tuple(sorted(teams, key=lambda team: indexes[team])),
               filter(lambda teams: teams[0] != teams[1],
                      product(teams, repeat=2)))
    n = len(teams)

    while True:
        y = list(islice(prod, 0, n-1))

        if y:
            yield y
        else:
            break


class TestCase(unittest.TestCase):
    def check(self, result, nobjects, ncolours):
        results = list(result)
        colors = set(map(lambda result: result[1], results))

        self.assertEqual(len(results), nobjects)
        print(results)
        self.assertEqual(len(colors), ncolours)

    def test_void(self):
        self.check(colorise([], list(range(10))), 0, 0)

    def test_fully_linked(self):
        graph = [
            ['a', 'b', 'c'],
        ]
        self.check(colorise(graph, list(range(10))), 3, 3)

    def test_fully_linked_non_connected(self):
        graph = [
            ['a', 'b', 'c'],
            ['d', 'e', 'f'],
        ]
        self.check(colorise(graph, list(range(10))), 6, 3)

    def __fixme__test_not_enough_colours(self):
        graph = [
            ['a', 'b', 'c']
        ]
        self.assertRaises(ValueError, list, colorise(graph, list(range(2))))

    def test_countries(self):
        graph = [
            ['Russia', 'Belarus', 'Ukraine'],
            ['Russia', 'Kazakhstan', 'China'],
            ['Germany', 'Poland', 'Czech Republic'],
            ['Poland', 'Belarus', 'Ukraine'],
        ]
        self.check(colorise(graph, ('red', 'green', 'yellow')),
                   len(set(chain(*graph))),
                   3)

    def test_championship(self):
        sched = list(championship(['team1', 'team2', 'team3', 'team4']))
        expected = [[('team1', 'team2'), ('team1', 'team3'), ('team1', 'team4')],
                    [('team1', 'team2'), ('team2', 'team3'), ('team2', 'team4')],
                    [('team1', 'team3'), ('team2', 'team3'), ('team3', 'team4')],
                    [('team1', 'team4'), ('team2', 'team4'), ('team3', 'team4')]]
        self.assertEqual(sched, expected)

    def __test_time(self):
        graph = [sample(range(10000), randint(2, 3)) for _ in range(10000)]
        colors = list(range(100))

        t = Timer()
        with t:
            colors = list(colorise(graph, colors))

        print(t.delta)
        print(colors)
