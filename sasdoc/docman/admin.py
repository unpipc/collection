from django.contrib import admin
import models

admin.site.register(models.UserProfile, models.UserProfileAdmin)
admin.site.register(models.Document, models.DocumentAdmin)
admin.site.register(models.Track, models.TrackAdmin)
admin.site.register(models.Baseline, models.BaselineAdmin)
admin.site.register(models.Revision, models.RevisionAdmin)
admin.site.register(models.Reason, models.ReasonAdmin)
admin.site.register(models.DocumentUpdate, models.DocumentUpdateAdmin)
admin.site.register(models.Event, models.EventAdmin)
admin.site.register(models.RelativeEvent, models.RelativeEventAdmin)
admin.site.register(models.History, models.HistoryAdmin)
admin.site.register(models.Tag, models.TagAdmin)
admin.site.register(models.Collection, models.CollectionAdmin)
admin.site.register(models.Shipment, models.ShipmentAdmin)
admin.site.register(models.Note)
admin.site.register(models.ActionPoint, models.ActionPointAdmin)

from django.contrib.contenttypes.models import ContentType
from django.core import urlresolvers

