import utils
from django.core.urlresolvers import resolve, reverse

class Menu(object):
    def __init__(self, **kwargs):
        self.title = kwargs.get('title', '')
        self.name = kwargs.get('name', utils.to_slug(self.title) or None)

        items = kwargs.get('subs', list())
        self.subs = filter(lambda x: isinstance(x, Menu), items)

        self.url = kwargs.get('url', "#")

        # Must be set to True explicitly.
        self.active = False

    def activate(self, *names):
        if names:
            name = names[0]

            self.active = (self.name == name)

            other = names[1:]

            if other:
                for s in self.subs:
                    s.activate(*other)

class SearchMenu(Menu):
    def __init__(self, request, **kwargs):
        super(SearchMenu, self).__init__(**kwargs)

        self.request = request

        self.model = kwargs.get('model')
        self.search_by = kwargs.get('search_by', utils.Qs())
        self.search_query = utils.Qs(self.search_by)
        self.search_query.update(
                {'searchobject': [self.model.__name__],
                 'searchname': [self.name]})

        self.url = self.search_query.with_url(reverse('Search'))


class MenuList(list):
    def __init__(self, *items):
        menu_items = filter(lambda x: isinstance(x, Menu), items)

        super(MenuList, self).__init__(menu_items)

    def activate(self, *names):
        map(lambda x: x.activate(*names), self)
