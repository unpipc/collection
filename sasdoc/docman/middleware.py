from docman import models, views

class RequestMiddleware(object):
    def process_request(self, request):
        models.REQUEST = request
        views.MENU = views.get_menu(request)
        views.SEARCH_MENU = views.get_search_menu(request)
