from django.db.models import Q
from django import forms
from django.utils.datastructures import SortedDict
from django.utils.safestring import mark_safe
from django.db.models import fields

import utils

QNONE = Q(pk=None) & ~Q(pk=None)

def get_field_or_manager(model, field_name):
    name = field_name
    field = None

    try:
        field = filter(lambda f: f.name == name, model._meta.fields)[0]
    except IndexError:
        # Field may be not physical, e.g. m2m.
        # Check if appropriate attribute (manager) exists in the model.
        manager = getattr(model, name, None)

        if manager:
            field = manager.field

    return field

def model_name(model):
    n = model._meta.verbose_name
    return n[0].upper() + n[1:]

def model_class_name(model):
    return model.__name__

def field_name(field):
    return field.capitalize()

class BaseOperator(object):
    def __init__(self, opstr):
        self.opstr = opstr

    def is_string(self, value):
        if isinstance(value, str) or isinstance(value, unicode):
            return True
        else:
            return False

    def with_postfix(self, main_str=""):
        return "%s__%s" % (main_str, self.opstr)

    def cast(self, value):
        pass

    def __unicode__(self):
        return self.opstr

class IterableOperator(BaseOperator):
    def cast(self, value):
        if utils.isiterable(value):
            return value
        else:
            return [value]

class ScalarOperator(BaseOperator):
    def cast(self, value):
        if self.is_string(value):
            return value

        if utils.isiterable(value):
            return value[0]
        else:
            return value

class BoolOperator(ScalarOperator):
    def __init__(self):
        super(BoolOperator, self).__init__("exact")

    def cast(self, value):
        value = super(BoolOperator, self).cast(value).lower()

        true = ('true', 'on', 'yes')
        false = ('false', 'off', 'no')

        if value in true:
            return True
        else:
            if value in false:
                return False
            else:
                try:
                    value = int(value)
                except ValueError:
                    raise forms.ValidationError("Unsupported value for bool type: %s" % value)

                return value > 0

Operator_bool = BoolOperator()
Operator_exact = ScalarOperator("__exact")
Operator_icontains = ScalarOperator("icontains")
Operator_in = IterableOperator("in")

def get_search_field(operator, base, *args, **kwargs):
    class SearchField(base):
       op = operator

    return SearchField(*args, **kwargs)


class SearchFieldsFactory(object):
    def __init__(self, meta=None):
        self.fmap = dict()
        self.fmap['default'] = get_search_field(Operator_icontains, forms.CharField)

        overrides = getattr(meta, 'overrides', None)

        if overrides:
            self.update(overrides)

    def get_field(self, model_field, **kwargs):
        mf = model_field

        kind = type(mf).__name__

        if hasattr(mf, 'choices') and mf.choices:
            kind += '_choice'

        related = False

        if hasattr(mf, 'field'):
            related = True
            related_model = mf.field.related.parent_model

            if isinstance(mf.field.rel, fields.related.ManyToOneRel):
                #related_operator = Operator_in
                pass

            if isinstance(mf.field.rel, fields.related.ManyToManyRel):
                #related_operator = Operator_exact
                pass

        if hasattr(mf, 'related'):
            related = True

            if isinstance(mf.related.field.rel, fields.related.ManyToOneRel):
                related_model = mf.related.parent_model

            if isinstance(mf.related.field.rel, fields.related.ManyToManyRel):
                related_model = mf.related.model

        f = None

        select_widget = kwargs.pop('select-widget', 'checkbox')

        if select_widget == "checkbox":
            widget = forms.CheckboxSelectMultiple()

        if select_widget == "list":
            widget = forms.SelectMultiple()

        if related:
            f = get_search_field(Operator_in, forms.ModelMultipleChoiceField, 
                    queryset=related_model.objects.all(), 
                    widget=widget, **kwargs)

        if kind == 'CharField_choice':
            f = get_search_field(Operator_in, forms.MultipleChoiceField, 
                    choices=mf.choices, 
                    widget=widget, **kwargs)
        
        if not f:
            f = self.fmap.get(kind, self.fmap['default'])

        f.required = False

        return f

class NegationFormMetaclass(type(forms.Form)):
    def __new__(cls, cls_name, bases, attrs):
        bundle_name = attrs.get('bundle_name')

        if bundle_name:
            negate_name = "%s_negate" % bundle_name
        else:
            negate_name = "negate"

        attrs[negate_name] = get_search_field(Operator_bool, forms.BooleanField, label='Not', required=False)

        def get_negation(self): 
            return self.cleaned_data[negate_name]

        attrs['get_negation'] = get_negation

        def get_negation_field(self): 
            return self[negate_name]

        def get_negation_field_name(self): 
            return negate_name

        attrs['get_negation_field'] = get_negation_field
        attrs['get_negation_field_name'] = get_negation_field_name

        def get_data_fields(self):
            data_fields = dict(self.fields)
            data_fields.pop(negate_name)

            return data_fields

        attrs['get_data_fields'] = get_data_fields

        return super(NegationFormMetaclass, cls).__new__(cls, cls_name, bases, attrs)

    def __init__(cls, cls_name, bases, attrs):
        def get_q_object(self):
            # To validate.
            super(cls, self).get_q_object()

            q = dict()

            data_fields = self.get_data_fields().items()
            #print "Data fields: %s" % data_fields
            #print "Cleaned data: %s" % self.cleaned_data
            for name, f in data_fields:
                if self.cleaned_data.has_key(name):
                    value = self.cleaned_data[name]

                    if value:
                        q[f.op.with_postfix(name)] = value

            if self.get_negation():
                rq = ~Q(**q)
            else:
                rq = Q(**q)

            #print "q-obj from %s: %s" % (cls.bundle_name, rq)
            return rq

        cls.get_q_object = get_q_object

        super(NegationFormMetaclass, cls).__init__(cls_name, bases, attrs)


class BaseForm(forms.Form):
    def __init__(self, data={}):
        self.path = list()
        self.level = 0
        super(BaseForm, self).__init__(data)

    def _prepend_path(self, chunk):
        self.path.insert(0, chunk)
        self.level += 1

    def _prepend_prefix(self, pre):
        if pre:
            if self.prefix is None:
                self.prefix = pre
            else:
                self.prefix = "%s_%s" % (pre, self.prefix)

class TypeCastForm(BaseForm):
    def with_prefix(self, s):
        if self.prefix:
            ps = "%s-%s" % (self.prefix, s)
        else:
            ps = s

        return ps

    def is_this_valid(self):
        for name, field in self.fields.items():
            pname = self.with_prefix(name)

            #print "key: %s" % pname
            if self.data.has_key(pname):
                tmp = self.data[pname]
                self.data[pname] = field.op.cast(self.data[pname])
                #print "Cast: %s -> %s" % (tmp, self.data[pname])

        return super(TypeCastForm, self).is_valid()

class NegationBaseForm(TypeCastForm):
    def get_q_object(self):
        if not self.is_valid():
            raise forms.ValidationError("Form not valid: %s." % self.errors)

    def data_fields(self):
        neg = self.get_negation_field_name()

        return filter(lambda f: f != neg, self.fields)

    def as_table(self):
        neg = self.get_negation_field()
        data_fields = self.data_fields()
        nfields = len(data_fields)
        invert_td_class = 'invert'
        label_td_class = 'label'
        field_td_class = 'field'

        tr = "<tr>%s</tr>"
        rows = ""

        for i, fname in enumerate(data_fields):
            f = self[fname]

            row = "<td class='%s'>%s</td>" % (label_td_class, f.label_tag())
            row += "<td class='%s'>%s</td>" % (field_td_class, f)

            if i == 0:
                row += "<td rowspan='%d' class='%s'>%s</td>" % (nfields, invert_td_class, neg)

            rows += tr % row

        return mark_safe(rows)

def get_negation_form_class(attrs):
    return NegationFormMetaclass('NegationForm', (NegationBaseForm,), attrs)

class SearchFormMetaclass(NegationFormMetaclass):
    def __new__(cls, cls_name, bases, attrs):
        attrs = SortedDict(attrs)

        meta = attrs.get('Meta')

        if not meta:
            Exception("No 'Meta' in class %s" % cls)

        attrs['_search_meta'] = meta
        model = getattr(meta, 'model', None)

        if not model:
            Exception("No 'model' in Meta of class %s" % cls)

        bundle_form_classes = list()

        fields_factory = SearchFieldsFactory(meta)
        search_by = getattr(meta, 'search_by', tuple())
        r_classes = SortedDict()

        count = 0

        for search_item in search_by:
            count += 1
            search_attrs = dict()

            if isinstance(search_item, tuple) and search_item[1] != None:
                if len(search_item) == 2:
                    r_name, r_class = search_item
                else:
                    if len(search_item) == 3:
                        r_name, r_class, search_attrs = search_item
                    else:
                        continue
                
                labeled_by_user = search_attrs.has_key('label')
                label = search_attrs.pop('label', r_name)

                def get_form_maker(label, count, r_class):
                    def make_form(data, **kwargs):
                        kwargs.update({'label': label})
                        kwargs.update({'seqno': count})

                        return r_class(data, **kwargs)

                    return make_form

                r_classes[r_name] = get_form_maker(label, count, r_class)

            if isinstance(search_item, tuple) and search_item[1] == None:
                if len(search_item) == 3:
                        search_attrs = search_item[2]

                search_item = search_item[0]

            if isinstance(search_item, str):
                fname = search_item

                labeled_by_user = search_attrs.has_key('label')

                #if labeled_by_user:
                    #label = search_attrs.pop('label', fname)

                try:
                    field = filter(lambda f: f.name == fname, model._meta.fields)[0]
                except IndexError:
                    # Field may be not physical, e.g. m2m.
                    # Check if appropriate attribute (manager) exists in the model.
                    field = getattr(model, fname, None)

                    if field is None:
                        r_set = "%s_set" % fname
                        field = getattr(model, r_set, None)

                    if field is None:
                        # Weaken the condition to be able to search by related fields.
                        # E.g. revision__document__deadline
                        #raise Exception("No such field '%s' in model '%s'" % (fname, model))
                        pass

                form_field = fields_factory.get_field(field, **search_attrs)

                field_bundle = get_negation_form_class({fname: form_field, "bundle_name": fname, 'seqno': count})
                bundle_form_classes.append(field_bundle)


        attrs['bundle_form_classes'] = bundle_form_classes
        attrs['nested_classes'] = r_classes

        return super(SearchFormMetaclass, cls).__new__(cls, cls_name, bases, attrs)

class SearchForm(NegationBaseForm):
    __metaclass__ = SearchFormMetaclass

    class Meta:
        pass

    def __init__(self, data={}, **kwargs):
        labeled_by_user = kwargs.has_key('label')

        model = getattr(self._search_meta, 'model', None)
        form_label = None

        if labeled_by_user:
            form_label = kwargs['label']
        else:
            if model:
                form_label = model_name(model)

        self.seqno = kwargs.get('seqno')

        self.nested_forms = SortedDict()
        self.bundle_forms = list()

        cls = type(self)

        for name, F in cls.nested_classes.items():
            field = get_field_or_manager(model, name)

            verbose_name = name

            if field is not None:
                verbose_name = field.verbose_name

            f = F(data, label=verbose_name)
            
            f._prepend_prefix(name)
            self.nested_forms[name] = f

        #print "Bundles for %s are: %s" % (cls, cls.bundle_form_classes)
        #print "Nested for %s are: %s" % (cls, cls.nested_classes)

        for F in cls.bundle_form_classes:
            f = F(data)
            self.bundle_forms.append(f)


        local_data = dict(data)
        super(SearchForm, self).__init__(local_data)
        self._prepend_path(form_label)


    def _prepend_path(self, chunk):
        super(SearchForm, self)._prepend_path(chunk)

        for form in self._get_child_forms():
            form._prepend_path(chunk)

    def get_errors(self):
        errors = []

        for form in self.bundle_forms:
            errors.append(form.errors)

        for form in self.nested_forms.values():
            errors.append(form.errors)

        return errors

    def is_this_valid(self):
        valid = super(SearchForm, self).is_this_valid()

        for form in self.bundle_forms:
            v = form.is_this_valid()

            if valid and not v:
                valid = v

        return valid

    def is_valid(self):
        valid = self.is_this_valid()

        for x, form in self.nested_forms.items():
            # Force call is_valid() for each one for
            # them to get .cleaned_data attribute created.
            v = form.is_valid()

            if valid and not v:
                valid = v

        return valid

    def queryset(self):
        qs = self._get_queryset()

        # None means that search is not limited.
        if qs is None:
            qs = self._search_meta.model.objects.all()
        else:
            # Group by PK:
            qs = self._search_meta.model.objects.filter(pk__in=qs.values('pk').annotate())

        return qs

    def _get_queryset(self):
        def r(q):
            if not q:
                # Search dict is empty (or None). 
                # Do not limit the search and return None.
                return None
            else:
                return self._search_meta.model.objects.filter(q)

        if not self.is_this_valid():
            raise forms.ValidationError("Form not valid.")

        q = Q()

        for form in self.bundle_forms:
            q = q & form.get_q_object()

        for rname, rf in self.nested_forms.items():
            qs = rf._get_queryset()

            if not qs is None:
                q = q & Q(**{Operator_in.with_postfix(rname): qs})

        return r(q)
    
    def get_path_str(self):
        return "/".join(self.path)

    def get_short_path_str(self):
        if self.level == 1:
            return self.get_path_str()
        else:
            chunk = self.path[-1]

            if chunk is None:
                return None
            else:
                #return "...%s" % chunk
                return chunk

    def _prepend_prefix(self, pre):
        super(SearchForm, self)._prepend_prefix(pre)

        for form in self._get_child_forms():
            form._prepend_prefix(pre)

    def __hidden_fields(self):
        if self.level != 1:
            return ""

        class HiddenData(forms.Form):
            searchobject = forms.CharField(widget=forms.HiddenInput, 
                    initial=model_class_name(self._search_meta.model))

        hd = HiddenData()

        return hd.as_table()

    def as_table(self):
        neg = self.get_negation_field()

        output = self.__hidden_fields()

        tr = "<tr>%s</tr>"
        colspan = 2

        if self.level == 1:
            #th_cap = "<th class='form-caption' colspan='%d'>%s</th>" % (colspan, 'Search fields')
            th_cap = "<th class='form-caption' colspan='%d'></th>" % colspan
            th_invert = "<th class='form-caption-negate'>%s</th>" % "Invert"
            output += mark_safe(tr % str(th_cap + th_invert))

        print_label = False

        for form in self._get_child_forms():
            if isinstance(form, SearchForm):
                print_label = True

            if print_label:
                if isinstance(form, SearchForm):
                    label = form.get_short_path_str()
                else:
                    label = self.get_short_path_str()
                    print_label = False

                td = "<td class='form-label'>%s:</td>" % label
                output += tr % td

            output += form.as_table()

            if isinstance(form, SearchForm):
                print_label = True

        return mark_safe(output)

    def __iter__(self):
        return self.__search_iter__(True)

    def __all_fields(self):
        return self.__search_iter__(False)

    def _get_child_forms(self):
        all_forms = self.bundle_forms + self.nested_forms.values()
        count = 1
        max_count = len(all_forms)

        for i in range(1, max_count+1):
            form = filter(lambda f: f.seqno == i, all_forms)[0]

            yield form

    def __search_iter__(self, skip_negation_fields=True):
        pass
