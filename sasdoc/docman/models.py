import utils
import hashlib
import re
from django.conf import settings
from django import forms
from django.db import models, transaction
from django.db.models import Q, F, Count
from django.contrib import admin, auth
from django.contrib.contenttypes import models as ct_models, generic
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.admin.options import InlineModelAdmin
from django.utils import unittest
from django.utils.decorators import method_decorator
from django.utils.safestring import mark_safe
from django.utils import timezone as tz
from django.shortcuts import get_object_or_404
from django.db.models import signals
from django.db.models.query import QuerySet
from django.core import exceptions
from django.core.urlresolvers import reverse
import forms as docman_forms

import pytz
from datetime import datetime, date
import time

# Auto-set by middleware or admin form.
REQUEST = None

# Europe/>>>>>Stockholm<<<<<
TZ = settings.TIME_ZONE.split("/")[1]

def path_change_tab(path, tab):
    parts = path.split('/')
    parts[-1] = tab

    return "/".join(parts)

def is_user_cm(user):
    G = auth.models.Group

    try:
        cm = G.objects.get(name="CM")
    except exceptions.ObjectDoesNotExist:
        return False
    
    return cm in user.groups.all()

class AdminEdit(models.Model):
    class Meta:
        abstract = True

    def __init__(self, *args, **kwargs):
        super(AdminEdit, self).__init__(*args, **kwargs)

        self.admin_url_change = utils.get_admin_url(self, 'change')
        self.__class__.admin_url_add = utils.get_admin_url(self, 'add')

class OrderedQuerySet(QuerySet):
    def order_on_demand(self, request, namespace, **kwargs):
        tabs = kwargs.get('tabs', False)
        overriden_default_keys = kwargs.get('default_sorting_keys')

        fields_map = getattr(self.model, 'fields_map', dict())

        if overriden_default_keys:
            default_sorting_keys = overriden_default_keys
        else:
            default_sorting_keys = getattr(self.model, 'default_sorting_keys', dict())

        query = utils.Qs(request)
        q = query.get_subquery(namespace)

        q_fields = q.get('order_by')

        def no_leading_sign(s):
            if s.startswith('-') or s.startswith('+'):
                return s[1:]
            else:
                return s

        def fields_comparator(a, b):
            return no_leading_sign(a) ==  no_leading_sign(b)

        d_fields = tuple(default_sorting_keys)
        ordering_items = utils.unique_join(q_fields, d_fields, comparator=fields_comparator)
        unsigned_ordering_items = map(no_leading_sign, ordering_items)

        fields = list()

        for i in ordering_items:
            if not i:
                continue

            if fields_map.has_key(i):
                fields += fields_map[i]
            else:
                fields.append(i)

        ordered_qs = self.order_by(*fields)

        ordered_qs.ordering_info = dict()

        model_fields = [f.name for f in self.model._meta.fields]
        ordering_candidates = utils.unique_join(ordering_items, fields_map.keys(), model_fields, comparator=fields_comparator)

        asc = 'aA'
        desc = 'Aa'
        unord = 'unord'

        ordering_number = 0
        max_ordering_number = len(ordering_items)

        for field in ordering_candidates:
            f = no_leading_sign(field)
            ordered_qs.ordering_info[f] = dict()

            order = 'unord'
            asc_field = f
            desc_field = "%s%s" % ("-", f)

            if f in unsigned_ordering_items:
                if field.startswith('-'):
                    order = desc
                else:
                    order = asc

                ordering_number += 1

            if tabs:
                path = path_change_tab(request.path_info, namespace)
            else:
                path = request.path_info

            a_tag_id = "%s_%s" % (namespace, f)
            a_tag = '<a id="%(tag_id)s" href="%(path)s?%(namespace)s:order_by=%%(field)s#%(tag_id)s">%%(order)s</a>' % \
                            {'path': path, 'namespace': namespace, 'tag_id': a_tag_id}

            asc_html = a_tag % {'field': asc_field, 'order': asc}
            desc_html = a_tag % {'field': desc_field, 'order': desc}

            if order == asc:
                if max_ordering_number > 1:
                    fmt = "%%s-%d&nbsp;%%s" % ordering_number

                    if ordering_number > 1:
                        a = asc_html
                    else:
                        a = asc

                    d = desc_html
                else:
                    fmt = "%s&nbsp;%s"
                    a = asc
                    d = desc_html

            if order == desc:
                if max_ordering_number > 1:
                    fmt = "%%s&nbsp;%%s-%d" % ordering_number

                    if ordering_number > 1:
                        d = desc_html
                    else:
                        d = desc

                    a = asc_html
                else:
                    fmt = "%s&nbsp;%s"

                    a = asc_html
                    d = desc

            if order == unord:
                fmt = "%s&nbsp;%s"
                a = asc_html
                d = desc_html

            html = fmt % (a, d)
            html = "<span class='ordering_info'>%s</span>" % html
            ordered_qs.ordering_info[f]['html'] = mark_safe(html)

        return ordered_qs

class OrderedManager(models.Manager):
    use_for_related_fields = True

    def get_query_set(self):
        return OrderedQuerySet(self.model)

class SmartOrderingMixin(models.Model):
    class Meta:
        abstract = True

    objects = OrderedManager()

class M2mHook(object):
    def __init__(self, model, m2m_field_name):
        self.model = model
        self.m2m_field_name = m2m_field_name

    def hook(self, sender, **kwargs):
        action = kwargs.get('action')
        reverse = kwargs.get('reverse')

        instance = kwargs.get('instance')
        pk_set = kwargs.get('pk_set')

        if action in ('pre_remove', 'pre_add'):
            return

        add = set()
        add_and_revert = set()
        remove = set()

        m2m_field = getattr(instance, self.m2m_field_name)
        m2m_model = getattr(self.model, self.m2m_field_name).field.related.parent_model

        if action == 'pre_clear':
            instance._cleared = set(m2m_field.values_list('pk', flat=True))
            return

        if action == 'post_clear':
            remove = instance._cleared

        if action == 'post_add':
            if hasattr(instance, '_cleared') and instance._cleared:
                add_and_revert = instance._cleared & pk_set
                add = pk_set - add_and_revert

                instance._cleared = set()
            else:
                add = pk_set

        if action == 'post_remove':
            remove = pk_set

        remove_objects = m2m_model.objects.filter(pk__in=remove)
        add_objects = m2m_model.objects.filter(pk__in=add)
        add_and_revert_objects = m2m_model.objects.filter(pk__in=add_and_revert)

        func_name = "%s_changed" % self.m2m_field_name
        func = getattr(instance, func_name)

        func(remove_objects, action='remove')
        func(add_objects, action='add')
        func(add_and_revert_objects, action='add', revert=True)


STYLES = {
    'regular': 'regular',
    'blocked': 'blocked',
    'archived': 'archived',
    'in_progress': 'in-progress',
    'in_progress_expired': 'in-progress-expired'}

def name_field(*args, **kw):
    return models.CharField(max_length=32, *args, **kw)

def slug_field():
    return models.CharField(unique=True, max_length=32, editable=False)

def verbose_choice(value, choice_data):
    l = map(lambda y: y[1], filter(lambda x: x[0] == value, choice_data))

    if l:
        return l[0]
    else:
        return ""

def i_represent(iterator, request, *args, **kwargs):
    return map(lambda o: o.represent(request, *args, **kwargs), iterator)

def represent_objects(request, qs, **kwargs):
    ordering_namespace = kwargs.get('ordering_namespace')
    default_sorting_keys = kwargs.get('default_sorting_keys')

    shallow = kwargs.get('shallow', False)
    tabs = kwargs.get('tabs', False)

    if ordering_namespace:
        qs = qs.order_on_demand(request, ordering_namespace, tabs=tabs, default_sorting_keys=default_sorting_keys)

    objects = dict()
    objects['count'] = qs.count()
    objects['objects'] = i_represent(qs, request, shallow=shallow)

    if ordering_namespace:
        objects['ordering_info'] = qs.ordering_info

    return objects

class History(models.Model):
    class Meta:
        verbose_name_plural = "Histories"
        ordering = ('-timestamp',)

    content_type = models.ForeignKey(ct_models.ContentType)
    object_id = models.PositiveIntegerField()
    #object_id = models.CharField(max_length=64)
    history_object = generic.GenericForeignKey()

    author = models.ForeignKey('UserProfile')
    message = models.TextField()
    timestamp = models.DateTimeField(auto_now=True)
    message_id = models.PositiveIntegerField(null=True, blank=True)

    def __unicode__(self):
        return "%s. %s (%s)" % (self.timestamp, self.message, self.author)

    def represent(self, request, **kwargs):
        self.author_profile = self.author.represent(request, **kwargs)

        return self

class WithNiceId(models.Model):
    class Meta:
        abstract = True

    ID_WIDTH = 4

    @classmethod
    def get_lookup_methods(cls):
        # Tell 'get_object()' method how an object
        # must be searched. Possible values are:
        # 'pk'          -- filter by pk=id
        # 'prefixed_id' -- Cut off prefix to get id and filter by pk=id.
        #                  E.g. DOC0024 -> filter by pk=24.
        # 'custom'      -- Call get_q_object_by_name() method.

        # Default is empty list to not accidently
        # make objects available for users via get_object() method.
        return tuple()

    @classmethod
    def get_object(cls, unknown_type_id, **kwargs):
        methods = cls.get_lookup_methods()

        obj = None

        for method in methods:
            if method in ('pk', 'prefixed_id'):
                if method == 'prefixed_id':
                    pk = cls.niceid_to_pk(unknown_type_id)
                else:
                    pk = unknown_type_id

                if pk is not None:
                    try:
                        obj = cls.objects.get(pk=pk)
                    except (exceptions.ObjectDoesNotExist, ValueError):
                        # PK with wrong format generates ValueError
                        pass
                    else:
                        break

            if method == 'custom':
                try:
                    q = cls.get_q_object_by_name(unknown_type_id)

                    if q:
                        obj = cls.objects.select_related().get(q)
                        # More options for select_related()/prefetch_related() might
                        # be placed here.
                except exceptions.ObjectDoesNotExist:
                    pass

        return obj

    def nice_id(self):
        cls = type(self)
        methods = cls.get_lookup_methods()

        # Prefer prefixed ID over regular PKs.:

        if 'prefixed_id' in methods:
            prefix = self.niceid_prefix()
            fmt = "%%s%%.%dd" % WithNiceId.ID_WIDTH # E. g. "%s%.4s"

            return fmt % (prefix, self.pk) # E. g. "DOC0024"

        if 'pk' in methods:
            return self.pk

        # For 'custom' method, this routine must be overriden.

    @classmethod
    def niceid_to_pk(cls, niceid):
        prefix = cls.niceid_prefix()

        id = None

        if prefix:
            if niceid.startswith(prefix):
                number = niceid[len(prefix):]
                m = re.compile("\d{%s}" % WithNiceId.ID_WIDTH).match(number)

                if m: id = m.string

        return id

    def unsaved(self):
        # Tell whether the object is saved in the DB.
        # Assume by default that PK is an integer 
        # equal to 'None' when the object is unsaved.
        # Descendants are to override the implementation
        # according to their PK type.
        return self.pk == None

    @classmethod
    def niceid_prefix(cls):
        # Prefix to cut off of the nice id to
        # convert it to real PK.
        # It is supposed that descendants will provide
        # own implementations.

        # Return "" by default to cut off nothing and
        # allow addressing an object by an ugly id 
        # pretending nice.
        # Or if descendant model has nice PK and
        # no conversion is needed, then this default 
        # way is just OK and not to be overriden.
        return ""

class WithHistory(WithNiceId):
    class Meta:
        abstract = True

    CREATED = "Created."
    UPDATED = "Updated."

    digest = models.CharField(max_length=128)
    history_messages = generic.GenericRelation(History)

    def dump(self):
        return unicode(self)

    def record(self, message, m_id=None, extra=None):
        if REQUEST and not REQUEST.user.is_anonymous():
            profile = REQUEST.user.get_profile()
        else:
            return

        if message[-1] != ".":
            message += "."

        if extra:
            message = "%s (%s)" % (message, extra)

        History.objects.create(message_id=m_id, history_object=self, message=message, author=profile)

    def revert_last_record(self, m_id):
        this_ct = ct_models.ContentType.objects.get_for_model(self)

        qs = History.objects.filter(content_type=this_ct, object_id=self.pk, message_id=m_id).order_by('-timestamp')

        if qs.count():
            self.history_messages
            qs[0].delete()

    def raw_dump(self):
        return utils.obj_dump(self)

    def make_digest(self):
        m = hashlib.md5()
        m.update(self.raw_dump())

        return m.hexdigest()

    def save(self, message=None, *args, **kwargs):
        if self.unsaved():
            just_created = True
        else:
            just_created = False

        extra = kwargs.pop('extra', None)

        old_digest = self.digest
        self.digest = self.make_digest()

        super(WithHistory, self).save(*args, **kwargs)

        if just_created:
            self.record("%s %s" % (WithHistory.CREATED, self.dump()))

        if message:
            self.record(message, extra)
        else:
            if not just_created:
                if old_digest != self.digest:
                    self.record("%s %s" % (WithHistory.UPDATED, self.dump()), extra)

    def m2m_changed(self, objects, **kwargs):
        action = kwargs.get("action")
        revert = kwargs.get("revert", False)

        if action == "add":
            msg = "+"
        else:
            if action == "remove":
                msg = "-"
            else:
                raise Exception("Wrong action: %s" % action)

        for o in objects:
            if revert:
                self.revert_last_record(o.pk)
                o.revert_last_record(self.pk)
            else:
                o_record = "%s%s" % (msg, unicode(o))
                self.record(o_record, o.pk)

                self_record = "%s%s" % (msg, unicode(self))
                o.record(self_record, self.pk)

    def represent(self, request, **kwargs):
        records = self.history_messages.all()

        self.log_records = represent_objects(request, records, shallow=True, **kwargs)

        return self

class Note(models.Model):
    class Meta:
        ordering = ('-timestamp',)

    content_type = models.ForeignKey(ct_models.ContentType)
    object_id = models.PositiveIntegerField()
    #object_id = models.CharField(max_length=64)
    note_object = generic.GenericForeignKey()

    author = models.ForeignKey("UserProfile")
    message = models.TextField()
    timestamp = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "%s. %s (%s)" % (self.timestamp, self.message, self.author)

    def represent(self, request, **kwargs):
        self.author_profile = self.author.represent(request, shallow=True)

        return self

class WithNotes(models.Model):
    class Meta:
        abstract = True

    with_notes = True
    notes = generic.GenericRelation(Note)

    def note(self, message):
        if not REQUEST:
            raise Exception("REQUEST is not available.")

        Note.objects.create(note_object=self, message=message, author=REQUEST.user.get_profile())

    def represent(self, request, **kwargs):
        objects = self.notes.all()
        self.related_notes = represent_objects(request, objects, shallow=True)

        #self.related_notes = dict()
        #self.related_notes['count'] = objects.count()
        #self.related_notes['objects'] = objects

        self.note_form = docman_forms.get_comment_form()

        return self

class Tag(WithHistory, AdminEdit, SmartOrderingMixin):
    # Do not use slug_field() here caus it gives
    # non-editable field.
    slug = models.CharField(unique=True, max_length=32)

    default_sorting_keys = ('slug',)

    def __unicode__(self):
        return self.slug

    @classmethod
    def get_lookup_methods(cls):
        return ('custom',)

    @classmethod
    def get_q_object_by_name(cls, slug):
        return Q(slug__exact=slug)

    def nice_id(self):
        return self.slug

    @classmethod
    def get_objects(cls, search_name):
        objects = None
        annotation = None

        if search_name == 'all':
            objects = cls.objects.all()
            annotation = 'All Tags'

        if objects is not None:
            objects = objects.prefetch_related()

        return (objects, annotation, search_name)

    def save(self, **kwargs):
        self.slug = utils.to_slug(self.slug, upper=True)

        super(Tag, self).save(**kwargs)

    def represent(self, request, **kwargs):
        shallow = kwargs.pop('shallow', False)

        self.no_tags_paragraph = True

        if not shallow:
            stuff = (
                    ('documents', 'document', Document),
                    ('action_points', 'action_point', ActionPoint),
                    ('revisions', 'revision', Revision),
                    ('updates', 'update', DocumentUpdate),
                    ('relative_events', 'relative_event', RelativeEvent),
                    ('events', 'event', Event),
                    ('users', 'user', UserProfile)
                    )

            for postfix, namespace, model in stuff:
                attr = "linked_%s" % postfix

                objects = model.objects.filter(tags=self)

                d = represent_objects(request, objects, shallow=True, ordering_namespace=namespace, **kwargs)

                setattr(self, attr, d)

        return super(Tag, self).represent(request)


class WithTag(WithHistory):
    class Meta:
        abstract = True

    tags = models.ManyToManyField(Tag, blank=True, null=True)

    def represent(self, request, **kwargs):
        self.linked_tags = self.tags.all()

        return super(WithTag, self).represent(request)

    def tags_changed(self, tags, **kwargs):
        super(WithTag, self).m2m_changed(tags, **kwargs)

class UserProfile(WithTag, WithNotes, AdminEdit, SmartOrderingMixin):
    # Do not offer CM profile creation admin operation.
    # Profiles are auto-created at user creation/registering.
    no_create = True

    user = models.OneToOneField(auth.models.User)

    fields_map = {
            'username': ['user__username'],
            '-username': ['-user__username'],
            'first_name': ['user__first_name'],
            '-first_name': ['-user__first_name'],
            'last_name': ['user__last_name'],
            '-last_name': ['-user__last_name'],
            }

    default_sorting_keys = ('first_name', 'last_name')

    def __unicode__(self):
        return unicode(self.get_full_name())

    @classmethod
    def get_lookup_methods(cls):
        return ('custom',)

    @classmethod
    def get_q_object_by_name(cls, name):
        return Q(user__username=name)

    def nice_id(self):
        return self.user.username

    @classmethod
    def get_objects(cls, search_name):
        objects = None
        annotation = None

        if search_name == 'all':
            objects = cls.objects.all()
            annotation = 'All Users'

        has_aps = Q(actionpoint__state=ActionPoint.OPEN)

        if search_name == 'with_aps':
            objects = cls.objects.filter(has_aps)
            annotation = 'Users with open Action Points'

        has_updates = Q(documentupdate__state__in=DocumentUpdate.active_states)

        if search_name == 'with_ups':
            objects = cls.objects.filter(has_updates)
            annotation = 'Users with current Document Updates'

        if search_name == 'unassigned':
            objects = cls.objects.filter(~Q(has_updates), ~Q(has_aps))
            annotation = 'Users with neither Document Updates nor Action Points assigned'

        if objects is not None:
            objects = objects.select_related()

        return (objects, annotation, search_name)

    def dump(self):
        full_name = self.get_full_name()

        if full_name:
            full_name = "%s (%s %s)" % (self.user.username, self.user.first_name, self.user.last_name)
        else:
            full_name = self.user.username

        no_dump_on_user_fields = ('id', 'password', 'last_login', 'date_joined')
        dump = utils.obj_dump(self.user, verbose=True, no_fields=no_dump_on_user_fields)

        return "%s. %s" % (full_name, utils.sentence(dump))

    def get_full_name(self):
        if self.user.first_name or self.user.last_name:
            full_name = "%s %s" % (self.user.first_name, self.user.last_name)
        else:
            full_name = self.user.username

        return full_name.strip()

    def get_full_name_html(self):
        full_name = "<span title='%s'>%s %s</span>" % (self.user.username, self.user.first_name, self.user.last_name)

        return full_name.strip()

    def raw_dump(self):
        #return utils.obj_dump(self.user) + super(UserProfile, self).raw_dump()
        return self.dump()

    def represent(self, request, **kwargs):
        shallow = kwargs.pop('shallow', False)

        self.username = self.user.username
        self.first_name = self.user.first_name
        self.full_name = self.get_full_name()
        self.last_name = self.user.last_name
        self.email = self.user.email

        if is_user_cm(self.user):
            self.role = "Configuration Manager"
        else:
            self.role = "User"

        if not shallow:
            related_updates = DocumentUpdate.objects.filter(assignees=self)
            self.related_updates = represent_objects(request, related_updates, shallow=True, ordering_namespace='update', **kwargs)

            self.actionpoints = represent_objects(request, self.actionpoint_set.all(), shallow=True, ordering_namespace='ap', **kwargs)
            super(UserProfile, self).represent(request)
            WithNotes.represent(self, request)

        self.has_current_updates = self.documentupdate_set.filter(state__in=DocumentUpdate.active_states).exists()
        self.has_current_aps = self.actionpoint_set.filter(state=ActionPoint.OPEN).exists()

        klass = "non-firm"
        hints = list() 

        if self.has_current_updates:
            hints.append("Has incomplete Updates assigned.")

        if self.has_current_aps:
            hints.append("Has open Action Points assigned.")

        if not hints:
            hints.append("Has no current activities.")
            klass = "firm"

        hints_str = " ".join(hints)

        hints1 = hints + ["Username %s." % self.username]
        hints1_str = " ".join(hints1)

        username_html = "<span class='%s' title='%s'>%s</span>" % (klass, hints_str, self.username)
        self.username_html = mark_safe(username_html)

        full_name_html = "<span class='%s' title='%s'>%s</span>" % (klass, hints1_str, self.full_name)
        self.full_name_html = mark_safe(full_name_html)

        return self

def create_user_profile(instance, created, **kwargs):
    profile, created = UserProfile.objects.get_or_create(user=instance)

    if not created:
        # To log changes.
        profile.save()

signals.post_save.connect(create_user_profile, sender=auth.models.User)

class ActionPoint(WithTag, WithNotes, AdminEdit, SmartOrderingMixin):
    title = models.CharField(max_length=64, unique=True)

    OPEN = '1'
    CLOSED = '2'
    STATE = ((OPEN, 'Open'), (CLOSED, 'Closed'))

    state = models.CharField(max_length=3, choices=STATE, default=OPEN)
    
    deadline = models.DateField()
    assignee = models.ForeignKey('UserProfile')

    linked_document = models.ForeignKey('Document', null=True, blank=True)
    linked_revision = models.ForeignKey('Revision', null=True, blank=True)
    linked_update = models.ForeignKey('DocumentUpdate', null=True, blank=True)

    fields_map = {
            '-linked_document': ['-linked_document__name'], 
            '+linked_document': ['+linked_document__name'], 
            '-linked_revision': ['-linked_revision__number'], 
            '+linked_revision': ['+linked_revision__number'], 
            '-linked_update': ['-linked_update__pk'], 
            '+linked_update': ['+linked_update__pk'], 
            }

    default_sorting_keys = ('state', 'deadline', 'assignee', 'title')

    @classmethod
    def niceid_prefix(cls):
        return "AP"

    @classmethod
    def get_lookup_methods(cls):
        return ('prefixed_id',)

    def __unicode__(self):
        linked = list()

        if self.linked_document:
            linked.append(unicode(self.linked_document))

        if self.linked_revision:
            linked.append(unicode(self.linked_revision))

        if self.linked_update:
            linked.append(unicode(self.linked_update))

        if linked:
            linked_str = ". Linked with %s" % ", ".join(linked)
        else:
            linked_str = ""

        return "%s: %s (%s)%s" % (self.nice_id(), self.title, self.get_state_str(), linked_str)

    def get_state_str(self):
        return verbose_choice(self.state, ActionPoint.STATE)
    
    def set_state(self, state):
        self.state = state
        self.save()
    
    def get_deadline_html(self):
        warn_expiration = self.state == ActionPoint.OPEN

        if warn_expiration:
            class_expired='expired'
        else:
            class_expired='due'

        html, hint, warn_class = utils.format_date(self.deadline, warn_expiration=True, 
                class_expired=class_expired, html=True)

        return html

    @classmethod
    def get_objects(cls, search_name):
        objects = None
        annotation = None

        if search_name == 'all':
            objects = cls.objects.all()
            annotation = 'All Action Points'

        search_by_state = None

        if search_name == 'open':
            search_by_state = cls.OPEN

        if search_name == 'closed':
            search_by_state = cls.CLOSED

        if search_by_state:
            objects = cls.objects.filter(state=search_by_state)
            annotation = 'Action Points in state "%s"' % verbose_choice(cls.OPEN, cls.STATE)

        if objects is not None:
            objects = objects.select_related().prefetch_related('assignee__documentupdate_set')

        return (objects, annotation, search_name)

    def represent(self, request, **kwargs):
        shallow = kwargs.pop('shallow', False)

        stuff = (
                ('documents', 'document', Document),
                ('revisions', 'revision', Revision),
                ('updates', 'update', DocumentUpdate),
                )

        for postfix, namespace, model in stuff:
            objects = model.objects.filter(actionpoint=self)

            setattr(self, postfix, objects)

        self.deadline_html = self.get_deadline_html()
        self.state_str = self.get_state_str()

        self.assignee_object = self.assignee.represent(request, shallow=True, **kwargs)

        super(ActionPoint, self).represent(request)
        return WithNotes.represent(self, request)

class WithURL(models.Model):
    class Meta:
        abstract = True

    url = models.URLField(blank=True, null=True)

class EventBase(SmartOrderingMixin, WithTag, AdminEdit, WithNotes):
    class Meta:
        abstract = True

    def get_title(self):
        return "%s: %s" % (self.nice_id(), self.title)

    @classmethod
    def get_lookup_methods(cls):
        return ('prefixed_id',)

    @classmethod
    def get_expired(cls, **kwargs):
        within_days = kwargs.get('within_days')
        before_date = kwargs.get('before_date')

        if not within_days and not before_date:
            raise Exception("Arguments: either 'within_days' or 'before_date'.")

        if within_days:
            expire_date = date.today() + tz.timedelta(days=within_days)
            objs = cls.objects.filter(date__lte=expire_date, date__gt=date.today())

        if before_date:
            objs = cls.objects.filter(date__lte=before_date)

        return objs

    def date_str(self):
        html, hint, warn_class = utils.format_date(self.date)

        return html

    def date_html(self, warn=False):
        if warn:
            class_expired = 'expired'
        else:
            class_expired = 'due'

        html, hint, warn_class = utils.format_date(self.date, warn_expiration=True, 
                class_expired=class_expired, html=True)

        return mark_safe(html)

class Event(EventBase):
    class Meta:
        ordering = ('date',)

    title = models.CharField(max_length=64, unique=True)
    date = models.DateField()
    days = models.IntegerField()

    default_sorting_keys = ('date', 'title')

    def date_str(self):
        html, hint, warn_class = utils.format_date(self.date)

        return html

    def save(self, **kw):
        self.days = time.mktime(self.date.timetuple()) / 60 / 60 / 24

        super(Event, self).save(**kw)

        self.create_relative()

    def as_html(self):
        formatted, hint, warn_class = utils.format_date(self.date, html=True)
        
        return mark_safe("%s: %s &mdash; %s" % (self.nice_id(), formatted, self.title))

    def __unicode__(self):
        formatted, hint, warn_class = utils.format_date(self.date)

        return "%s: %s - %s" % (self.nice_id(), formatted, self.title)

    @classmethod
    def niceid_prefix(cls):
        return "E"

    def create_relative(self):
        if not self.relativeevent_set.filter(event_type__exact=RelativeEvent.ABSOLUTE).exists():
            RelativeEvent.objects.create(title=self.title, event_base=self, event_type=RelativeEvent.ABSOLUTE)

    def represent(self, request, **kwargs):
        shallow = kwargs.pop('shallow', False)

        related = self.relativeevent_set.all()

        if not shallow:
            self.related_events = represent_objects(request, related, shallow=True, ordering_namespace='event', **kwargs)

        super(Event, self).represent(request)
        return WithNotes.represent(self, request)

class RelativeEventRelativeManager(OrderedManager):
    def get_query_set(self):
        qs = super(RelativeEventRelativeManager, self).get_query_set()
        
        return qs.filter(event_type__exact=RelativeEvent.RELATIVE)

class RelativeEvent(EventBase):
    objects = OrderedManager()
    relative_events = RelativeEventRelativeManager()

    ABSOLUTE='Absolute'
    RELATIVE='Relative'

    title = models.CharField(max_length=64, unique=True)
    event_base = models.ForeignKey(Event)
    shift = models.IntegerField(default=0, verbose_name="Shift (days)")
    date = models.DateField()

    fields_map = {
            'base_title': ['event_base__title'], 
            '-base_title': ['-event_base__title'], 
            'base_date': ['event_base__date'], 
            '-base_date': ['-event_base__date'], 
            }

    default_sorting_keys = ('-date', 'shift', 'title')

    event_type = models.CharField(max_length=16, default=RELATIVE)

    @classmethod
    def niceid_prefix(cls):
        return "RE"

    @classmethod
    def get_objects(self, search_name, **kwargs):
        objects = None
        annotation = None

        if search_name == 'all' or search_name == '':
            annotation = "All Events"
            objects = RelativeEvent.objects.all()

        if search_name == 'expired':
            annotation = "Expired Events"
            objects = RelativeEvent.get_expired(before_date=date.today())

        if search_name == 'within':
            try:
                if search_name == 'within':
                    attrs = kwargs.get('search_attributes', '')
                    days = utils.parse_url(attrs, '/', (int, '.+'), (str, 'days'))[0]

                    if days < 1:
                        raise Exception("'days' must be >= 1")
            except Exception:
                return (None, None, None)

            days_text = utils.days_phrase(days)

            annotation = "Events that will occur within next %s" % days_text
            objects = RelativeEvent.get_expired(within_days=days)
            search_name = "%s%d" % (search_name, days)

        if objects is not None:
            objects = objects.select_related()

        return (objects, annotation, search_name)

    def save(self, **kwargs):
        self.date = self.event_base.date + tz.timedelta(days=self.shift)

        super(RelativeEvent, self).save(**kwargs)

    def shift_html(self):
        if self.shift == 0:
            return mark_safe("&mdash;")

        if abs(self.shift) > 1:
            days = "days"
        else:
            days = "day"

        return "%+d %s" % (self.shift, days)

    def __unicode__(self):
        if self.event_type == RelativeEvent.ABSOLUTE:
            return unicode(self.event_base)

        if self.shift == 0:
            when = "by"
        else:
            if abs(self.shift) == 1:
                days = "day"
            else:
                days = "days"

            if self.shift > 0:
                after = "after"
            else:
                after = "before"

            when = "%d %s %s" % (abs(self.shift), days, after)

        return "%s: %s - %s - [%s %s]" % (self.nice_id(), self.date_str(), self.title, when, self.event_base.title)

    def represent(self, request, **kwargs):
        shallow = kwargs.pop('shallow', False)
        related = DocumentUpdate.objects.filter(deadline_id=self.id)

        if not shallow:
            self.related_updates = represent_objects(request, related, shallow=True, ordering_namespace="update", **kwargs)
            related = self.event_base.relativeevent_set.exclude(pk=self.pk)

            self.related_events = represent_objects(request, related, shallow=True, ordering_namespace="event", **kwargs)

        return super(RelativeEvent, self).represent(request)


class Revision:
    # Forward reference.
    pass

class DocumentRevisionManager(OrderedManager):
    def __init__(self):
        self.model = Revision
        super(DocumentRevisionManager, self).__init__()

    def get_query_set(self):
        qs = super(DocumentRevisionManager, self).get_query_set()
        return qs

class Document(WithTag, WithURL, WithNotes, AdminEdit, SmartOrderingMixin):
    default_sorting_keys = ('name', 'title')

    EXT="Ext"
    INT="Int"
    SCOPE = ((EXT, 'External'), (INT, 'Internal'))
    scope = models.CharField(max_length=16, choices=SCOPE, default=EXT)

    name = models.CharField(max_length=16, unique=True)

    title = models.CharField(max_length=64)
    number = models.CharField(max_length=64, unique=True)

    class Meta:
        unique_together = ('name', 'title')

    def save(self, *args, **kwargs):
        self.name = utils.to_slug(self.name, upper=True)
        super(Document, self).save(*args, **kwargs)

    @classmethod
    def get_lookup_methods(cls):
        return ('custom',)

    def nice_id(self):
        return self.name

    @classmethod
    def get_q_object_by_name(cls, name_or_number):
        return Q(name__exact=name_or_number) | Q(number__exact=name_or_number)
        #return cls.objects.get(cond)

    @classmethod
    def get_objects(self, search_name):
        objects = None
        annotation = None
        doc_scope = None
        doc_name = None

        if search_name == 'all' or search_name == '':
            annotation = "All Documents"
            objects = Document.objects.all()
        elif search_name in ('external', 'internal'):
            annotation = "%s Documents" % verbose_choice(doc_scope, Document.SCOPE)
            doc_scope = search_name.title()[:3]
            objects = Document.objects.filter(scope__exact=doc_scope)
        else:
            prefix = "name:"
            lp = len(prefix)
            prefix_in_url = search_name[:lp]

            if prefix == prefix_in_url.lower():
                doc_name = search_name[lp:]
                search_name = "name:%s" % doc_name

            if doc_name:
                annotation = "%s Documents" % doc_name
                objects = Document.objects.filter(name__exact=doc_name)

        if objects is not None:
            objects = objects.prefetch_related('revision_set', 'actionpoint_set')

        return (objects, annotation, search_name)

    def __unicode__(self):
        return self.name

    def u_title_num_name(self):
        return mark_safe("%s, %s &mdash; %s:%s" % (self.title, self.number, self.name, self.scope))

    def u_short(self):
        return self.name

    def dump(self):
        return "%s, %s - (%s:%s)" % (self.title, self.number, self.name, self.scope)

    def represent(self, request, **kwargs):
        shallow = kwargs.pop('shallow', False)

        self.revisions = represent_objects(request, 
                self.revision_set.all(), 
                shallow=shallow, 
                ordering_namespace='revision', 
                default_sorting_keys=('-number',),
                **kwargs)

        # Must have been prefetched, so do not filter(state=ActionPoint.OPEN)
        self.actionpoints = self.actionpoint_set.all()

        self.has_current_updates = filter(lambda r: r.has_current_updates, self.revisions['objects']) != list()
        self.has_current_aps = filter(lambda ap: ap.state == ActionPoint.OPEN, self.actionpoints) != list()

        klass = "non-firm"
        hints = list()

        if self.has_current_updates:
            hints.append("Has current target Updates.")

        if self.has_current_aps:
            hints.append("Has open Action Points.")

        if not hints:
            hints.append("Has no current activities.")
            klass = "firm"

        hints_str = " ".join(hints)

        number_html = "<span class='%s' title='%s'>%s</span>" % (klass, hints_str, self.number)
        name_html = "<span class='%s' title='%s'>%s</span>" % (klass, hints_str, self.name)

        self.number_html = mark_safe(number_html)
        self.name_html = mark_safe(name_html)

        if not shallow:
            self.updates = represent_objects(request, 
                    DocumentUpdate.objects.filter(from_revision__document=self), 
                    shallow=False, 
                    ordering_namespace='update',
                    **kwargs)

            self.actionpoints = represent_objects(request, self.actionpoints, shallow=True, ordering_namespace='ap', **kwargs)
            WithNotes.represent(self, request)


        self.scope_str = verbose_choice(self.scope, Document.SCOPE)

        super(Document, self).represent(request)

        return self

class Track(WithTag, WithNotes, AdminEdit, SmartOrderingMixin):
    name = name_field(unique=True)
    date_of_pra = models.ForeignKey(RelativeEvent)

    def __unicode__(self):
        return self.name

    default_sorting_keys = ('date_of_pra', 'name')

    def nice_id(self):
        return self.name

    @classmethod
    def get_lookup_methods(cls):
        return ('custom',)

    @classmethod
    def get_q_object_by_name(cls, name):
        return Q(name__exact=name)

    @classmethod
    def get_objects(cls, search_name):
        objects = None
        annotation = None

        if search_name == 'all':
            objects = cls.objects.all()
            annotation = "All Tracks"

        if objects is not None:
            objects = objects.select_related()

        return (objects, annotation, search_name)

    def represent(self, request, **kwargs):
        shallow = kwargs.pop('shallow', False)

        date_str = self.date_of_pra.date_str()

        formatted = utils.format_date(self.date_of_pra.date, warn_expiration=True, 
                hint_fmt="%s. %%s" % date_str, class_expired='due', 
                text=self.date_of_pra.title, html=True)

        self.date_of_pra_html, hint, warn_class = formatted

        self.shipments = represent_objects(request, self.shipment_set.all(), shallow=shallow, ordering_namespace='shipment', **kwargs)

        self.has_current_updates = filter(lambda sh: sh.has_current_updates, self.shipments['objects']) != list()

        if self.has_current_updates:
            name_html = "<span class='non-firm' title='Has current target Updates'>%s</span>" % self.name
        else:
            name_html = "<span class='firm' title='Firm'>%s</span>" % self.name

        self.name_html = mark_safe(name_html)
        super(Track, self).represent(request)

        return WithNotes.represent(self, request)


#RelativeEvent(EventMixin, SmartOrderingMixin, WithNotes, WithTag, AdminEdit):
#  class Event(EventMixin, SmartOrderingMixin, WithTag, AdminEdit, WithNotes):
class Baseline(WithTag, WithNotes, AdminEdit, SmartOrderingMixin):
    name = name_field(unique=True)

    def __unicode__(self):
        return self.name

    @classmethod
    def get_lookup_methods(cls):
        return ('custom',)

    def nice_id(self):
        return self.name

    @classmethod
    def get_q_object_by_name(cls, name):
        return Q(name__exact=name)

    default_sorting_keys = ('name',)

    @classmethod
    def get_objects(cls, search_name):
        objects = None
        annotation = None

        if search_name == 'all':
            objects = cls.objects.all()
            annotation = "All Baselines"

        if objects is not None:
            objects = objects.select_related()

        return (objects, annotation, search_name)


    def represent(self, request, **kwargs):
        shallow = kwargs.pop('shallow', False)

        self.revisions = represent_objects(request, self.revision_set.all(), shallow=shallow, ordering_namespace='revision', **kwargs)

        self.has_current_updates = filter(lambda r: r.has_current_updates, self.revisions['objects']) != list()

        if self.has_current_updates:
            name_html = "<span class='non-firm' title='Has revisions with current target Updates!'>%s</span>" % self.name
        else:
            name_html = "<span class='firm' title='All revisions are firm'>%s</span>" % self.name

        self.name_html = mark_safe(name_html)

        super(Baseline, self).represent(request)
        return WithNotes.represent(self, request)

class Revision(WithTag, WithNotes, AdminEdit, SmartOrderingMixin):
    class Meta:
        unique_together = ('number', 'document')
        ordering = ('-number',)

    number = name_field()
    document = models.ForeignKey(Document)

    #Revision may be planned to be created till PRA of the release,
    # not for a certain shipment. So, optional.
    shipment = models.ForeignKey('Shipment', blank=True, null=True)

    # Optional since might be not latest in all baselines.
    baselines = models.ManyToManyField(Baseline, blank=True, null=True)

    fields_map = {
            'doc_name': ['document__name'], 
            '-doc_name': ['-document__name'],
            'doc_number': ['document__number'], 
            '-doc_number': ['-document__number']
            }

    default_sorting_keys = ('doc_name', '-number')

    #def save(self, *args, **kwargs):
    #    self.name = "%s:%s" % (self.document.name, self.number)
    #
    #    super(Revision, self).save(*args, **kwargs)

    def __unicode__(self):
        return "%s:%s" % (self.document.name, self.number)

    def nice_id(self):
        return unicode(self)

    @classmethod
    def get_lookup_methods(cls):
        return ('custom',)

    @classmethod
    def get_q_object_by_name(cls, spec):
        bits = spec.split(':')

        if len(bits) == 2:
            doc_name, rev_number = bits

            return Q(document__name__exact=doc_name, number__exact=rev_number)

    @classmethod
    def get_objects(self, search_name, **kwargs):
        attrs = kwargs.get('search_attributes')
        objects = None
        annotation = None
        DU = DocumentUpdate

        if search_name == 'all' or search_name == '':
            annotation = "All Revisions"
            objects = Revision.objects.all()

        if search_name == 'current_source':
            annotation = "Source Revisions for current Updates"
            objects = Revision.objects.filter(from_rev_in_docupdate__in=DocumentUpdate.current())

        if search_name == 'current_target':
            annotation = "Target Revisions for current Updates"
            objects = Revision.objects.filter(to_rev_in_docupdate__in=DocumentUpdate.current())

        if search_name == 'backlog':
            annotation = "Target Revisions of expired Updates"
            objects = Revision.objects.filter(to_rev_in_docupdate__in=DocumentUpdate.get_expired())

        if search_name == 'backlog':
            annotation = "Target Revisions of expired Updates"
            objects = Revision.objects.filter(to_rev_in_docupdate__in=DocumentUpdate.get_expired())

        if search_name == 'released':
            annotation = "Released Revisions (not aimed to by an Update in state %s or %s.)" % (DU.inprogress.name, DU.blocked.name)
            objects = Revision.objects.filter(~Q(to_rev_in_docupdate__in=DU.current()))

        if objects is not None:
            objects = objects.select_related()

        return (objects, annotation, search_name)

    @classmethod
    def get_report(self, **kwargs):
        start_date = kwargs.get('start_date')
        end_date = kwargs.get('end_date')

        if start_date is not None and end_date is not None:
            annotation = "Report as of %s and %s" % (start_date, end_date)

            # Those updates that were completed in the indicated period of time.
            q_approved = Q(to_rev_in_docupdate__done_date__gt=start_date) & \
                    Q(to_rev_in_docupdate__done_date__lte=end_date)

            reason = 'to_rev_in_docupdate__reasons__reason_type__exact'

            tr_approved = Revision.objects.filter(q_approved & Q(**{reason: Reason.TR}))
            cr_approved = Revision.objects.filter(q_approved & Q(**{reason: Reason.CR}))
            wp_approved = Revision.objects.filter(q_approved & Q(**{reason: Reason.WP}))
            other_approved = Revision.objects.filter(q_approved & Q(**{reason: Reason.OTHER}))

            DU = DocumentUpdate

            # Updates opened before the end date of the period 
            # that have not been completed at all or have been completed 
            # after the start date.
            q_inprogress = Q(to_rev_in_docupdate__open_date__lte=end_date) & \
                    (Q(to_rev_in_docupdate__state__in=DU.active_states) | \
                     (~Q(to_rev_in_docupdate__done_date=None) & Q(to_rev_in_docupdate__done_date__gt=start_date)) | \
                     (~Q(to_rev_in_docupdate__canceled_date=None) & Q(to_rev_in_docupdate__canceled_date__gt=start_date)) | \
                     (~Q(to_rev_in_docupdate__archived_date=None) & Q(to_rev_in_docupdate__archived_date__gt=start_date)) \
                    )

            tr_inprogress = Revision.objects.filter(q_inprogress & Q(**{reason: Reason.TR}))
            cr_inprogress = Revision.objects.filter(q_inprogress & Q(**{reason: Reason.CR}))
            wp_inprogress = Revision.objects.filter(q_inprogress & Q(**{reason: Reason.WP}))
            other_inprogress = Revision.objects.filter(q_inprogress & Q(**{reason: Reason.OTHER}))

            return ( map(lambda x: list(x.distinct()), (tr_approved, cr_approved, wp_approved, other_approved)),
                     map(lambda x: list(x.distinct()), (tr_inprogress, cr_inprogress, wp_inprogress, other_inprogress))
                   )


    def related_and_self(self):
        return self.document.revision_set.all()

    def related_to_this(self):
        return self.related_and_self().exclude(pk=self.pk)

    def is_blocked(self):
        return self.to_rev_in_docupdate.filter(state=DocumentUpdate.BLOCKED).count() > 0

    def dump(self):
        return "%s (%s)" % (unicode(self), self.shipment or "no shipment")

    def get_to_updates(self):
        return self.to_rev_in_docupdate.select_related()

    def get_from_updates(self):
        return self.from_rev_in_docupdate.select_related()

    def represent(self, request, **kwargs):
        shallow = kwargs.pop('shallow', False)

        if not shallow:
            to_updates = self.get_to_updates()
            self.to_updates = represent_objects(request, to_updates, shallow=True, ordering_namespace='to_update', **kwargs)

            from_updates = self.get_from_updates()
            self.from_updates = represent_objects(request, from_updates, shallow=True, ordering_namespace='from_update', **kwargs)

            self.baselines_objects = self.baselines.all()
            self.collection_objects = self.collection_set.all()

        self.blocked = self.is_blocked()

        if self.blocked:
            self.style_state = STYLES['blocked']
        else:
            self.style_state = STYLES['regular']

        italic_x = "<span style='font-style: italic'>x</span>"
        self.to_header = mark_safe("Updates %s&rarr;%s" % (italic_x, self.number))
        self.from_header = mark_safe("Updates %s&rarr;%s" % (self.number, italic_x))

        self.has_current_updates = self.to_rev_in_docupdate.filter(state__in=DocumentUpdate.active_states).exists()
        self.has_current_aps = self.actionpoint_set.filter(state=ActionPoint.OPEN).exists()

        klass = "non-firm"
        hints = list()

        if self.has_current_updates:
            hints.append("Has current target Updates.")

        if self.has_current_aps:
            hints.append("Has open Action Points linked.")

        if not hints:
            hints.append("Has no current activities.")
            klass = "firm"

        hints_str = " ".join(hints)

        number_html = "<span class='%s' title='%s'>%s</span>" % (klass, hints_str, self.number)

        self.number_html = mark_safe(number_html)

        super(Revision, self).represent(request)
        return WithNotes.represent(self, request)

    def add_baseline(self, baseline):
        self.baselines.add(baseline)

    def remove_baseline(self, baseline):
        # Do not call 'remove()' for objects
        # that are not bound. It will trigger m2m_changed
        # signal anyway.
        if self.baselines.filter(pk=baseline.pk):
            self.baselines.remove(baseline)

    def baselines_changed(self, baselines, **kwargs):
        action = kwargs.get("action")
        revert = kwargs.get("revert", False)

        if action == "add":
            msg = "On"

            for r in self.related_to_this():
                for b in baselines:
                    r.remove_baseline(b)

        super(Revision, self).m2m_changed(baselines, **kwargs)

def revision_pre_save(instance, **kwargs):
    try:
        old = Revision.objects.get(pk=instance.pk)
    except exceptions.ObjectDoesNotExist:
        #The change will be handled in 'revision_post_save':
        return

    new = instance

    if old.document != new.document:
        old.document.record("-rev. %s" % old.number)
        new.document.record("+rev. %s" % new.number)

    if old.shipment != new.shipment:
        # Shipment is optional.

        if old.shipment:
            old.shipment.record("-rev. %s" % old)

        if new.shipment:
            new.shipment.record("+rev. %s" % new)

def revision_post_save(instance, created, **kwargs):
    if created:
        if instance.document:
            instance.document.record("+rev. %s" % instance.number)

        if instance.shipment:
            instance.shipment.record("+%s" % instance)


def revision_post_delete(instance, **kwargs):
        if instance.document:
            instance.document.record("-rev. %s" % instance.number)

        if instance.shipment:
            instance.shipment.record("-%s" % instance)

signals.pre_save.connect(revision_pre_save, sender=Revision)
signals.post_save.connect(revision_post_save, sender=Revision)
signals.post_delete.connect(revision_post_delete, sender=Revision)

def ap_pre_save(instance, **kwargs):
    try:
        old = ActionPoint.objects.get(pk=instance.pk)
    except exceptions.ObjectDoesNotExist:
        #The change will be handled in 'ap_post_save':
        return

    new = instance
    obj_id = instance.nice_id()

    for fname in ('assignee', 'linked_document', 'linked_revision', 'linked_update'):
        old_field = getattr(old, fname)
        new_field = getattr(new, fname)

        if old_field != new_field:
            if old_field:
                old_field.record("-%s" % obj_id)

            if new_field:
                new_field.record("+%s" % obj_id)

def ap_post_save(instance, created, **kwargs):
    if created:
        obj_id = instance.nice_id()

        for fname in ('assignee', 'linked_document', 'linked_revision', 'linked_update'):
            field = getattr(instance, fname)

            if field:
                field.record("+%s" % obj_id)


def ap_post_delete(instance, **kwargs):
    obj_id = instance.nice_id()

    for fname in ('assignee', 'linked_document', 'linked_revision', 'linked_update'):
        field = getattr(instance, fname)

        if field:
            field.record("-%s" % obj_id)

signals.pre_save.connect(ap_pre_save, sender=ActionPoint)
signals.post_save.connect(ap_post_save, sender=ActionPoint)
signals.post_delete.connect(ap_post_delete, sender=ActionPoint)

def event_post_save(instance, **kwargs):
    relatives = RelativeEvent.objects.filter(event_base=instance)

    for r in relatives:
        # To recalculate 'days' field.
        r.save()

signals.post_save.connect(event_post_save, sender=Event)

class Collection(WithTag, WithNotes, AdminEdit, SmartOrderingMixin):
    name = name_field(unique=True)
    revisions = models.ManyToManyField(Revision, blank=True, null=True)

    default_sorting_keys = ('name',)

    def __unicode__(self):
        return "%s: %s" % (self.nice_id(), self.name)

    @classmethod
    def get_lookup_methods(cls):
        return ('prefixed_id',)

    @classmethod
    def niceid_prefix(cls):
        return "C"

    def revisions_changed(self, revisions, **kwargs):
        super(Collection, self).m2m_changed(revisions, **kwargs)

    @classmethod
    def get_objects(cls, search_name):
        objects = None
        annotation = None

        if search_name == 'all':
            objects = cls.objects.all()
            annotation = 'All Collections'

        return (objects, annotation, search_name)

    def represent(self, request, **kwargs):
        shallow = kwargs.pop('shallow', False)

        if not shallow:
            self.related_revisions = represent_objects(request, self.revisions.all(), shallow=True, ordering_namespace='revision', **kwargs)

        super(Collection, self).represent(request)
        return WithNotes.represent(self, request)

class Shipment(WithTag, WithNotes, AdminEdit, SmartOrderingMixin):
    name = name_field(unique=True)
    track = models.ForeignKey(Track)

    default_sorting_keys = ('name',)

    def __unicode__(self):
        return self.name

    @classmethod
    def get_lookup_methods(cls):
        return ('custom',)

    def nice_id(self):
        return self.name

    @classmethod
    def get_q_object_by_name(cls, name):
        return Q(name__exact=name)

    def dump(self):
        return "%s (%s)" % (unicode(self), self.track)

    @classmethod
    def get_objects(cls, search_name):
        objects = None
        annotation = None

        if search_name == 'all':
            objects = cls.objects.all()
            annotation = "All Shipments"

        if objects is not None:
            objects = objects.select_related()

        return (objects, annotation, search_name)

    def represent(self, request, **kwargs):
        shallow = kwargs.pop('shallow', False)

        self.revisions = represent_objects(request, self.revision_set.all(), shallow=shallow, ordering_namespace='content', **kwargs)

        self.has_current_updates = filter(lambda r: r.has_current_updates, self.revisions['objects']) != list()

        if self.has_current_updates:
            name_html = "<span class='non-firm' title='Has current target Updates'>%s</span>" % self.name
        else:
            name_html = "<span class='firm' title='Firm'>%s</span>" % self.name

        self.name_html = mark_safe(name_html)

        super(Shipment, self).represent(request)
        return WithNotes.represent(self, request)

def shipment_pre_save(instance, **kwargs):
    try:
        old = Shipment.objects.get(pk=instance.pk)
    except exceptions.ObjectDoesNotExist:
        #The change will be handled in 'shipment_post_save':
        return

    new = instance

    if old.track != new.track:
        old.track.record("-%s" % old.name)
        new.track.record("+%s" % new.name)

def shipment_post_save(instance, created, **kwargs):
    if created:
        instance.track.record("+%s" % instance.name)


def shipment_post_delete(instance, **kwargs):
        instance.track.record("-%s" % instance)

signals.pre_save.connect(shipment_pre_save, sender=Shipment)
signals.post_save.connect(shipment_post_save, sender=Shipment)
signals.post_delete.connect(shipment_post_delete, sender=Shipment)

class Reason(WithTag, WithNotes, AdminEdit, SmartOrderingMixin):
    description = models.CharField(max_length=256, unique=True)

    TR = 'tr'
    CR = 'cr'
    WP = 'wp'
    OTHER = 'other'

    TYPE = ((TR, "TR"),
            (CR, "CR"),
            (WP, "WP"),
            (OTHER, "Other"))
    reason_type = models.CharField(max_length=16, choices=TYPE)

    def __unicode__(self):
        return "%s (%s)" % (self.description, self.reason_type)

    @classmethod
    def get_lookup_methods(cls):
        return ('prefixed_id',)

    @classmethod
    def niceid_prefix(cls):
        return "R"

    def represent(self, request, **kwargs):
        shallow = kwargs.pop('shallow', False)

        if not shallow:
            self.related_updates = represent_objects(request, DocumentUpdate.objects.filter(reasons=self), 
                    ordering_namespace='reason', shallow=True, **kwargs)

        self.description_html = utils.pack_text(self.description, hint=self.description, limitchars=24, 
                hint_on_overflow=True)

        super(Reason, self).represent(request)
        return WithNotes.represent(self, request)

class DocumentUpdateManager(OrderedManager):
    def get_query_set(self):
        qs = super(DocumentUpdateManager, self).get_query_set()
        
        return qs.extra(select={'deadline_days': "SELECT docman_relativeevent.shift \
            + docman_event.days FROM docman_relativeevent, docman_event \
            WHERE docman_relativeevent.id = docman_documentupdate.deadline_id \
            and docman_event.id = docman_relativeevent.event_base_id"}).order_by('deadline_days')


class DocumentUpdate(WithTag, WithNotes, AdminEdit):
    objects = DocumentUpdateManager()

    class Meta:
        ordering = ('state', '-id')

    fields_map = {'document': ['from_revision__document__name'],
                  '-document': ['-from_revision__document__name'],
                  'base_rev': ['from_revision__number'],
                  '-base_rev': ['-from_revision__number'],
                  'target_rev': ['to_revision__number'],
                  '-target_rev': ['-to_revision__number'],
                  'deadline': ['deadline_days'],
                  '-deadline': ['-deadline_days']}

    default_sorting_keys = ('state', 'deadline')

    class State:
        def __init__(self, name, prio):
            self.name = name
            self.prio = prio

        def __str__(self):
            return self.name

    class StateTuple:
        def __init__(self, *states):
            self.states = tuple(states)

        def __str__(self):
            return ", ".join([s.name for s in self.states])

    blocked    = State('Blocked', '1')
    created    = State('Created', '2')
    inprogress = State('In Progress', '3')
    done       = State('Done', '4')
    canceled   = State('Canceled', '5')
    archived   = State('Archived', '6')
    deleted    = State('Deleted', '7')

    # Pre-underscored _STATE is for internal use.
    BLOCKED    = blocked.prio
    _CREATED   = created.prio
    INPROGRESS = inprogress.prio
    DONE       = done.prio
    CANCELED   = canceled.prio
    ARCHIVED   = archived.prio
    _DELETED   = deleted.prio

    active_states = (_CREATED, BLOCKED, INPROGRESS)
    active_states_objects = StateTuple(created, blocked, inprogress)

    active_unblocked_states = (_CREATED, INPROGRESS)
    active_unblocked_states_objects = StateTuple(created, inprogress)

    inactive_states = (DONE, CANCELED)
    inactive_states_objects = StateTuple(done, canceled)

    STATE = ((BLOCKED,    blocked.name),
             (_CREATED,   created.name),
             (INPROGRESS, inprogress.name),
             (DONE,       done.name),
             (CANCELED,   canceled.name),
             (ARCHIVED,   archived.name),
             (_DELETED,   deleted.name),
            )

    from_revision = models.ForeignKey(Revision, related_name='from_rev_in_docupdate')
    to_revision = models.ForeignKey(Revision, related_name='to_rev_in_docupdate')

    reasons = models.ManyToManyField(Reason)
    assignees = models.ManyToManyField(UserProfile)

    state = models.CharField(max_length=32, choices=STATE, default=_CREATED)
    deadline = models.ForeignKey(RelativeEvent)
    review_info = models.CharField(max_length=256, null=True, blank=True)

    open_date     = models.DateTimeField(blank=True, null=True)
    canceled_date = models.DateTimeField(blank=True, null=True)
    done_date     = models.DateTimeField(blank=True, null=True)
    archived_date = models.DateTimeField(blank=True, null=True)

    @classmethod
    def get_lookup_methods(cls):
        return ('prefixed_id',)

    @classmethod
    def niceid_prefix(cls):
        return "U"

    @classmethod
    def get_objects(self, name, **kwargs):
        attrs = kwargs.get('search_attributes')

        cls = DocumentUpdate
        objects = None
        annotation = None

        what = "Document Updates"

        if name == 'all':
            objects = cls.objects.all()
            annotation = "All %s" % what

        if name == 'current':
            objects = cls.current()
            annotation = "%s in states '%s' or '%s'." % (what, cls.inprogress.name, cls.blocked.name)

        if name == 'expired':
            try:
                days = utils.parse_url(attrs, "/", (int, '.+'), (str, 'days'))[0]
                if days < 0:
                    raise utils.MalformedUrlException(attrs)
            except utils.MalformedUrlException:
                return (None, None, None)

            name = "%s%d" % (name, days)
            objects = cls.get_expired(+days) & cls.current()

            if days:
                annotation = "%s in states '%s' or '%s' already expired and those going to be expired within next %s" % \
                        (what, cls.inprogress.name, cls.blocked.name, utils.days_phrase(days))
            else:
                annotation = "Expired %s" % what

        if name == 'blocked':
            objects = cls.blocked_updates()
            annotation = "%s in state '%s'" % (what, cls.blocked.name)

        if name == 'done':
            objects = cls.done_updates()
            annotation = "%s in state '%s'" % (what, cls.done.name)

        if name == 'canceled':
            objects = cls.canceled_updates()
            annotation = "%s in state '%s'" % (what, cls.canceled.name)

        if name == 'archived':
            objects = cls.archived_updates()
            annotation = "%s in state '%s'" % (what, cls.archived.name)

        if objects is not None:
            objects = objects.select_related('deadline', 'from_revision', 'from_revision__document', 'to_revision')
            #objects = objects.prefetch_related('assignees', 'reasons')
            objects = objects.prefetch_related('assignees__documentupdate_set', 'assignees__actionpoint_set', 'reasons')

        return (objects, annotation, name)

    @classmethod
    def current(cls):
        return DocumentUpdate.objects.filter(state__in=DocumentUpdate.active_states)

    @classmethod
    def get_expired(cls, days_from_today=0):
        expire_date = date.today() + tz.timedelta(days=days_from_today)

        return DocumentUpdate.objects.filter(deadline__date__lt=expire_date)

    @classmethod
    def blocked_updates(cls):
        return DocumentUpdate.objects.filter(state__exact=DocumentUpdate.BLOCKED)

    @classmethod
    def done_updates(cls):
        return DocumentUpdate.objects.filter(state__exact=DocumentUpdate.DONE)

    @classmethod
    def canceled_updates(cls):
        return DocumentUpdate.objects.filter(state__exact=DocumentUpdate.CANCELED)

    @classmethod
    def archived_updates(cls):
        return DocumentUpdate.objects.filter(state__exact=DocumentUpdate.ARCHIVED)

    def get_state_html(self, deadline_warn_class=None):
        style_cls = STYLES['regular']
        hint = None

        if self.state == DocumentUpdate.BLOCKED:
            style_cls = STYLES['blocked']
            # Do not use deadline's warn class here,
            # cause blocked Update is already 'red'.

        if self.state == DocumentUpdate.INPROGRESS:
            if self.deadline_warn_class == 'expired':
                style_cls = STYLES['in_progress_expired']
                hint = 'Deadline expired'
            else:
                style_cls = STYLES['in_progress']

        if self.state == DocumentUpdate.ARCHIVED:
            ctyle_cls = STYLES['archived']

        state_str = self.get_state()

        if hint:
            state_str = utils.dashed_underline(state_str)
            html = "<span class='%s' title='%s'>%s</span>" % (style_cls, hint, state_str)
        else:
            html = "<span class='%s'>%s</span>" % (style_cls, self.state_str)

        return (mark_safe(html), style_cls)

    def get_state(self, styled=False):
        state = verbose_choice(self.state, DocumentUpdate.STATE)

        if styled:
            return self.__formatted(state, self.get_state_style())
        else:
            return state
    get_state.short_description = "State"
    get_state.admin_order_field = "state"

    def get_state_style(self):
        style = 'color:black'

        if self.state in [DocumentUpdate.BLOCKED]:
            style = 'color:red; font-weight:bold'

        if self.state in [DocumentUpdate.INPROGRESS]:
            style = 'font-weight:bold'

        return style

    def block_unblock(self):
        if DocumentUpdate.objects.filter(to_revision=self.from_revision, state__in=DocumentUpdate.active_unblocked_states).exists():
            self.state = DocumentUpdate.BLOCKED
        else:
            if self.state in DocumentUpdate.active_states:
                self.state = DocumentUpdate.INPROGRESS

    def block_unblock_other(self):
        if self.state in DocumentUpdate.active_states:
            for u in DocumentUpdate.objects.filter(from_revision=self.to_revision, state__in=DocumentUpdate.active_unblocked_states):
                u.state = DocumentUpdate.BLOCKED
                u.save()
        else:
            for u in DocumentUpdate.objects.filter(from_revision=self.to_revision, state=DocumentUpdate.BLOCKED):
                u.state = DocumentUpdate.INPROGRESS
                u.save()

    def open(self):
        self.open_date = tz.now()

    def save(self, *args, **kwargs):
        if self.open_date is None:
            self.open()

        if self.state in self.active_states:
            self.block_unblock()

        super(DocumentUpdate, self).save(*args, **kwargs)

        self.block_unblock_other()

    def delete(self, **kwargs):
        super(DocumentUpdate, self).delete(**kwargs)

        self.state = DocumentUpdate._DELETED
        self.block_unblock_other()

    def do_cancel(self):
        if self.state in DocumentUpdate.active_states:
            self.state = DocumentUpdate.CANCELED
            self.canceled_date = tz.now()
            self.save()
    do_cancel.short_description = "%s -> %s" % (active_states_objects, canceled)

    def do_reopen(self):
        if self.state in [DocumentUpdate.CANCELED, DocumentUpdate.DONE]:
            self.state = DocumentUpdate.INPROGRESS
            self.open()
            self.save()
    do_reopen.short_description = "%s -> %s" % (StateTuple(canceled, done), inprogress)

    def do_done(self):
        if self.state == DocumentUpdate.INPROGRESS:
            self.state = DocumentUpdate.DONE
            self.done_date = tz.now()
            self.save()
    do_done.short_description = "%s -> %s" % (inprogress, done)

    def do_archive(self):
        if self.state in DocumentUpdate.inactive_states:
            self.state = DocumentUpdate.ARCHIVED
            self.archived_date = tz.now()
            self.save()
    do_archive.short_description = "%s -> %s" % (inactive_states_objects, archived)

    def document(self, styled=False):
        if styled:
            return self.__formatted(self.from_revision.document, self.get_state_style())
        else:
            return self.from_revision.document
    document.short_description = "Document"
    document.admin_order_field = "from_revision__document"

    def __formatted(self, what, style=None):
        if style:
            return mark_safe("<span style='%(style)s'>%(what)s</span>" % \
                    {'style': style, 'what': what})
        else:
            return what

    def from_revision_number(self, styled=False):
        return self.__formatted(self.from_revision.number, self.get_state_style())
    from_revision_number.short_description = "from revision"
    from_revision_number.admin_order_field = "from_revision__number"

    def to_revision_number(self, styled=False):
        return self.__formatted(self.to_revision.number, self.get_state_style())
    to_revision_number.short_description = "to revision"
    to_revision_number.admin_order_field = "to_revision__number"

    def get_assignees(self):
        return ", ".join([x.user.username for x in self.assignees.all()])
    get_assignees.short_description = "Assignees"
    get_assignees.admin_order_field = "assignees__user__username"

    def get_reasons(self):
        def packed(obj):
            return utils.pack_text(obj.description, limitchars=24)

        return ", ".join(map(packed, self.reasons.all()))

    get_reasons.short_description = "Reasons"

    def __unicode__(self):
        return u"%s: %s:%s>%s [%s]" % (self.nice_id(), self.from_revision.document, self.from_revision.number, self.to_revision.number, self.get_state())

    def reasons_changed(self, reasons, **kwargs):
        super(DocumentUpdate, self).m2m_changed(reasons, **kwargs)

    def assignees_changed(self, assignees, **kwargs):
        action = kwargs.get("action")
        revert = kwargs.get("revert", False)

        if action == "add":
            msg = "+"
        else:
            if action == "remove":
                msg = "-"
            else:
                raise Exception("Wrong action: %s" % action)

        for u in assignees:
            if revert:
                self.revert_last_record(u.pk)
                u.revert_last_record(self.pk)
            else:
                u_record = "%s%s" % (msg, unicode(u))
                self.record(u_record, u.pk)

                self_record = "%s%s" % (msg, unicode(self))
                u.record(self_record, self.pk)

    def represent(self, request, **kwargs):
        shallow = kwargs.pop('shallow', False)

        self.state_str = self.get_state(styled=False)
        self.document = self.to_revision.document

        # This will be the content of the tooltip.
        # No HTML is possible there, so use text method.
        date_str = self.deadline.date_str()

        if self.state in DocumentUpdate.active_states:
            class_expired='expired'
        else:
            # Do not attract attention if the Update is not
            # 'current'.
            class_expired='due'

        formatted = utils.format_date(self.deadline.date, warn_expiration=True, 
                hint_fmt="%s. %%s" % date_str, class_expired=class_expired, 
                text=self.deadline.title, html=True)

        self.deadline_html, self.deadline_hint, self.deadline_warn_class = formatted

        self.state_html, self.style_state = self.get_state_html(self.deadline_warn_class)

        if not shallow:
            all = DocumentUpdate.objects.all()

            related = all.filter(Q(to_revision=self.to_revision) | Q(from_revision=self.to_revision) |
                                 Q(to_revision=self.from_revision) | Q(from_revision=self.from_revision))

            self.related_updates = represent_objects(request, related.exclude(pk=self.pk), shallow=True, ordering_namespace='update', **kwargs)
            self.actionpoints = represent_objects(request, self.actionpoint_set.all(), shallow=True, ordering_namespace='ap', **kwargs)

            super(DocumentUpdate, self).represent(request)
            WithNotes.represent(self, request)

        self.has_current_aps = self.actionpoint_set.filter(state=ActionPoint.OPEN).exists()

        if self.has_current_aps:
            hint = "Has open Action Points linked."
            klass = "non-firm"
        else:
            hint = "Has no open Action Points linked."
            klass = "firm"

        id_html = "<span class='%s' title='%s'>%s</span>" % (klass, hint, self.nice_id())
        self.nice_id_html = mark_safe(id_html)

        self.reasons_objects = i_represent(self.reasons.all(), request, shallow=True, **kwargs)
        self.assignees_objects = i_represent(self.assignees.all(), request, shallow=True, **kwargs)

        return self

class ModelWithNiceIdAdmin(admin.ModelAdmin):
    def get_nice_id(self, o):
        return o.nice_id()
    get_nice_id.short_description = "ID"
    get_nice_id.admin_order_field = "id"

class ModelWithHistoryAdmin(ModelWithNiceIdAdmin):
    #FIXME: check whether actual.
    def get_form(self, request, obj=None, **kwargs):
        # Save request for further use in inline forms.
        # Inline forms do not have own `get_form', `save_model', etc. methods, 
        # so that there is no chance to have request in use.

        global REQUEST
        REQUEST = request

        return super(ModelWithHistoryAdmin, self).get_form(request, obj, **kwargs)

class ModelWithTagAdmin(admin.ModelAdmin):
    formfield_overrides = {
            models.ManyToManyField: {'widget': forms.CheckboxSelectMultiple()}
    }

class DocumentUpdateForm(forms.ModelForm):
    def clean_deadline(self):
            event = self.cleaned_data['deadline']

            if date.today() >= event.date:
                pass
                #Commented out: Impossible to edit object with expired deadline.
                #raise forms.ValidationError("Deadline occurs in the past.")

            return event

    def clean(self):
        try:
            from_revision = self.cleaned_data['from_revision']
            to_revision = self.cleaned_data['to_revision']
        except KeyError:
            # This happens when partially filled form is saved.
            return self.cleaned_data

        to_in_other_update = to_revision.to_rev_in_docupdate.all()

        if not self.instance is None:
            to_in_other_update = to_in_other_update.filter(~Q(pk=self.instance.pk))

        if to_in_other_update.exists():
            raise forms.ValidationError("'To' revision is the target in %s" % to_in_other_update[0])

        if from_revision == to_revision:
            raise forms.ValidationError("'To' revision is the same as 'From' revision")

        if to_revision.document_id != from_revision.document_id:
            raise forms.ValidationError("'To' and 'From' revisions' documents do not match")

        return self.cleaned_data


class PrintCharsInNameBaseForm(forms.ModelForm):
    def clean_name(self):
            name = self.cleaned_data['name']

            if re.compile(".*\s").match(name):
               raise forms.ValidationError("Name contains whitespace characters")

            return name

class BaselineBaseForm(PrintCharsInNameBaseForm):
    class Meta:
        model = Baseline

def get_baseline_change_form(baseline):
    class BaselineChangeForm(BaselineBaseForm):
        #class Meta:
        #    model = Baseline 

        revisions = forms.ModelMultipleChoiceField(widget=FilteredSelectMultiple("revisions", False),
                queryset=Revision.objects.all(), 
                initial=Revision.objects.filter(baselines__in=[baseline]),
                required=False)

        def __init__(self, *args, **kwargs):
            super(BaselineChangeForm, self).__init__(*args, **kwargs)

        def clean_revisions(self):
            selected_ids = self.cleaned_data['revisions']
            Q_selected = Q(pk__in=selected_ids)

            selected = Revision.objects.filter(Q_selected)
            deselected = Revision.objects.filter(~Q_selected)

            wrong_docs = set()

            for r in selected:
                selected_related = r.related_to_this() & selected

                if selected_related.exists():
                    wrong_docs.add(r.document)

            if wrong_docs:
                raise forms.ValidationError("More than one revision on the baseline for %s." % \
                        ",".join(["'%s'" % unicode(doc) for doc in wrong_docs]))

            return self.cleaned_data['revisions']

    return BaselineChangeForm


class TrackForm(PrintCharsInNameBaseForm):
    pass

class TrackAdmin(ModelWithHistoryAdmin, ModelWithTagAdmin):
    form = TrackForm
    list_display = ('get_name', 'date_of_pra')
    fields = ('name', 'date_of_pra', 'tags')
    search_fields = ('name',)

    def get_name(self, o):
        return o.name
    get_name.short_description = "Name"
    get_name.admin_order_field = 'name'

class ActionPointAdmin(ModelWithHistoryAdmin, ModelWithTagAdmin):
    list_display = ('get_title', 'get_deadline', 'get_state', 'get_assignee')
    fields = ('title', 'state', 'deadline', 'assignee', 'linked_document', 'linked_revision', 'linked_update', 'tags')
    search_fields = ('title',)

    actions = ['do_open', 'do_close']

    def get_title(self, o):
        return o.title
    get_title.short_description = "title"
    get_title.admin_order_field = 'title'

    def get_deadline(self, o):
        return o.deadline
    get_deadline.short_description = 'deadline'
    get_deadline.admin_order_field = 'deadline'

    def get_state(self, o):
        return o.get_state_str()
    get_state.short_description = 'state'
    get_state.admin_order_field = 'state'

    def get_assignee(self, o):
        return o.assignee
    get_assignee.short_description = 'assignee'
    get_assignee.admin_order_field = 'assignee'

    def do_open(self, request, queryset):
        for ap in queryset.all():
            ap.set_state(ActionPoint.OPEN)
    do_open.short_description = 'Set state to "Open"'

    def do_close(self, request, queryset):
        for ap in queryset.all():
            ap.set_state(ActionPoint.CLOSED)
    do_close.short_description = 'Set state to "Close"'

class BaselineAddForm(BaselineBaseForm):
    base_baseline = forms.ModelChoiceField(queryset=Baseline.objects.all(), required=False)

class BaselineAdmin(ModelWithHistoryAdmin, ModelWithTagAdmin):
    fields = ('name',)
    list_display = ('get_name', )
    search_fields = ('name',)

    formfield_overrides = {
            models.ManyToManyField: {'widget': forms.CheckboxSelectMultiple}
    }

    def get_name(self, o):
        return o.name
    get_name.short_description = "Name"
    get_name.admin_order_field = 'name'

    def get_form(self, request, baseline=None, **kwargs):
        if baseline is None:
            fields = ('name', 'base_baseline', 'tags')
            form = BaselineAddForm
        else:
            form = get_baseline_change_form(baseline)
            fields = ('name', 'revisions', 'tags')

        cls = type(self)
        
        cls.form = form
        cls.fields = fields

        return super(BaselineAdmin, self).get_form(request, baseline, **kwargs)

    #FIXME
    #@transaction.commit_on_success?
    def save_model(self, request, baseline, form, change):
        if not change:
            super(BaselineAdmin, self).save_model(request, baseline, form, change)

            base_baseline = form.cleaned_data.get('base_baseline')

            if base_baseline != None:
                for r in base_baseline.revision_set.all():
                    r.baselines.add(baseline)
        else:
            super(BaselineAdmin, self).save_model(request, baseline, form, change)

            before = set(baseline.revision_set.values_list('pk', flat=True))

            revisions = form.cleaned_data['revisions']

            # Previously on baseline:
            Q_on_baseline = Q(baselines__in=[baseline])

            on_baseline = Revision.objects.filter(Q_on_baseline)
            off_baseline = Revision.objects.filter(~Q_on_baseline)

            # Selected in the widget.
            Q_selected = Q(pk__in=revisions)

            selected = Revision.objects.filter(Q_selected)
            deselected = Revision.objects.filter(~Q_selected)

            for r in on_baseline & deselected:
                r.remove_baseline(baseline)

            for r in off_baseline & selected:
                r.add_baseline(baseline)


class DeadlineFilter(admin.SimpleListFilter):
    title = "deadline expiration"
    parameter_name = "expire"

    def lookups(self, request, model_admin):
        return (
                ('expired', 'Expired'),
                ('today', 'Today'),
                ('7days', 'Within 7 days'),
                ('later', 'Later'),
               )

    def queryset(self, request, queryset):
        dtnow = datetime.now()

        days_till_today = time.mktime(datetime.today().timetuple()) / 60 / 60 / 24

        if self.value() == 'expired':
            return queryset.filter(deadline__event_base__days__lte = days_till_today - F('deadline__shift'))

        if self.value() == 'today':
            return queryset.filter(deadline__event_base__days__gte = days_till_today - F('deadline__shift'),
                                   deadline__event_base__days__lt = days_till_today + 1 - F('deadline__shift'))

        if self.value() == '7days':
            return queryset.filter(deadline__event_base__days__gte = days_till_today - F('deadline__shift'),
                                   deadline__event_base__days__lt = days_till_today + 7 - F('deadline__shift'))

        if self.value() == 'later':
            return queryset.filter(deadline__event_base__days__gte = days_till_today + 7 - F('deadline__shift'))

class DocumentUpdateAdmin(ModelWithHistoryAdmin, ModelWithTagAdmin):
    model = DocumentUpdate
    form = DocumentUpdateForm

    fields = ('state', 'from_revision', 'to_revision', 'reasons', 'assignees', 'deadline', 'review_info', 'tags')

    list_display = ('get_nice_id', 'get_document', 'get_from_revision_number', 'get_to_revision_number', 
            'get_reasons', 'get_assignees', 'deadline', 'get_state')

    list_filter = ('from_revision__document__title', 'from_revision__document__name', 
            'state', 'open_date', DeadlineFilter)

    readonly_fields = ('state',)
    exclude = ('open_date', 'canceled_date', 'done_date', 'archived_date')

    #ordering = ('from_revision__document__title',)
    #ordering = ('state',)
    search_fields = ('from_revision__document__title', 
            'from_revision__document__number', 
            'from_revision__number', 
            'to_revision__number',
            'reasons__description',
            'assignees__user__username')

    def queryset(self, request):
        return DocumentUpdate.objects.all()

    def get_from_revision_number(self, o):
        return o.from_revision_number(styled=True)
    get_from_revision_number.short_description = DocumentUpdate.from_revision_number.short_description
    get_from_revision_number.admin_order_field = DocumentUpdate.from_revision_number.admin_order_field

    def get_to_revision_number(self, o):
        return o.to_revision_number(styled=True)
    get_to_revision_number.short_description = DocumentUpdate.to_revision_number.short_description
    get_to_revision_number.admin_order_field = DocumentUpdate.to_revision_number.admin_order_field

    def get_document(self, o):
        return o.document()
    get_document.short_description = DocumentUpdate.document.short_description
    get_document.admin_order_field = DocumentUpdate.document.admin_order_field

    def get_state(self, o):
        return o.get_state(styled=True)
    get_state.short_description = "State"
    get_state.admin_order_field = "state"

    def get_actions(self, request):
        """ With this method we'll be able to move 'delete_selected'
        action to the end of the list
        """
        actions = super(DocumentUpdateAdmin, self).get_actions(request)
        del actions['delete_selected']

        actions['done'] = (DocumentUpdateAdmin.done, 'done', DocumentUpdateAdmin.done.short_description)
        actions['cancel'] = (DocumentUpdateAdmin.cancel, 'cancel', DocumentUpdateAdmin.cancel.short_description)
        actions['reopen'] = (DocumentUpdateAdmin.reopen, 'reopen', DocumentUpdateAdmin.reopen.short_description)
        actions['archive'] = (DocumentUpdateAdmin.archive, 'archive', DocumentUpdateAdmin.archive.short_description)
        actions['delete_selected'] = (DocumentUpdateAdmin.delete_selected, 'delete_selected', DocumentUpdateAdmin.delete_selected.short_description)

        return actions

    def done(self, request, queryset):
        for o in queryset.all():
            o.do_done()
    done.short_description = DocumentUpdate.do_done.short_description

    def cancel(self, request, queryset):
        for o in queryset.all():
            o.do_cancel()
    cancel.short_description = DocumentUpdate.do_cancel.short_description

    def archive(self, request, queryset):
        for o in queryset.all():
            o.do_archive()
    archive.short_description = DocumentUpdate.do_archive.short_description

    def reopen(self, request, queryset):
        for o in queryset.all():
            o.do_reopen()
    reopen.short_description = DocumentUpdate.do_reopen.short_description

    def delete_selected(self, request, queryset):
        for o in queryset.all():
            o.delete()
    delete_selected.short_description = "Warning! Delete selected Document Updates. Consider 'Cancel'."

    def delete_model(self, obj):
        # This model requires explicit calling of delete() to
        # properly handle states.
        obj.delete()
    delete_model.short_description = delete_selected.short_description


class DocumentUpdateInline(admin.TabularInline):
    model = DocumentUpdate

class RevisionAdmin(ModelWithHistoryAdmin, ModelWithTagAdmin):
    fields = ('document', 'number', 'shipment', 'baselines', 'tags')

    list_filter = ('document__name',)
    list_display = ('number', 'get_document_title', 'get_document_name', 'get_shipment')

    search_fields = ('number', 'document__title', 'baselines__name')

    formfield_overrides = {
            models.ManyToManyField: {'widget': forms.CheckboxSelectMultiple}
    }

    def get_document_title(self, rev):
        return rev.document.title
    get_document_title.short_description = "Document title"
    get_document_title.admin_order_field = "document__title"

    def get_document_name(self, rev):
        return rev.document.name
    get_document_name.short_description = "Document name"
    get_document_name.admin_order_field = "document__name"

    def get_shipment(self, rev):
        return rev.shipment or "No shipment"
    get_shipment.short_description = "Shipment"
    get_shipment.admin_order_field = "shipment__name"

class RevisionInline(admin.TabularInline):
    model = Revision
    fields = ('number', 'shipment', 'baselines', 'tags')

    formfield_overrides = {
            # Changes nothing.
            models.ManyToManyField: {'widget': forms.CheckboxSelectMultiple()}
    }


#class TagInline(admin.TabularInline):
#    model = Document.tags.through

class CollectionAdmin(ModelWithHistoryAdmin, ModelWithTagAdmin):
    fields = ('name', 'revisions', 'tags')
    list_display = ('name',)
    filter_horizontal = ('revisions',)

class ShipmentForm(PrintCharsInNameBaseForm):
    pass

class ShipmentAdmin(ModelWithHistoryAdmin, ModelWithTagAdmin):
    form = ShipmentForm
    fields = ('name', 'track')
    list_display = ('name', 'track')


class DocumentAdmin(ModelWithHistoryAdmin, ModelWithTagAdmin):
    fields = ('title', 'number', 'name', 'scope', 'url', 'tags')
    list_display = ('title', 'number', 'name', 'scope', 'url')
    list_filter = ('name', 'scope')
    inlines = (RevisionInline,)

    search_fields = ('title', 'number')

class ReasonAdmin(ModelWithHistoryAdmin, ModelWithTagAdmin):
    fields = ('description', 'reason_type', 'tags')
    search_fields = ('description', 'reason_type')

class TagAdmin(ModelWithHistoryAdmin, ModelWithTagAdmin):
    fields = ('slug',)
    search_fields = ('slug',)

class HistoryAdmin(admin.ModelAdmin):
    search_fields = ('timestamp',)
    list_display = ('author', 'get_object', 'timestamp', 'message')
    list_display_links = ('message',)

    def get_object(self, o):
        return mark_safe("<a href='%s'>%s</a>" % (utils.get_admin_url(o.history_object), o.history_object))
    get_object.short_description = "Object"

    def get_timestamp(self, o):
        return o.timestamp
    get_timestamp.short_description = "Timestamp (%s)" % (settings.TIME_ZONE)
    get_timestamp.admin_order_field = 'timestamp'

class EventAdmin(ModelWithHistoryAdmin, ModelWithTagAdmin):
    exclude = ('days',)
    search_fields = ('title',)
    list_display = ('get_date',)

    fields = ('title', 'date', 'tags')

    def get_date(self, o):
        return unicode(o)
    get_date.short_description = "Date"
    get_date.admin_order_field = 'date'

class RelativeEventAdmin(ModelWithHistoryAdmin, ModelWithTagAdmin):
    model = RelativeEvent
    exclude = ('event_type',)

    fields = ('title', 'event_base', 'shift', 'tags')
    search_fields = ('title', 'event_base__title')

    def queryset(self, request):
        return RelativeEvent.relative_events.all()

class UserProfileAdmin(ModelWithHistoryAdmin):
    model = UserProfile

    fields = ('tags',)

class Test(unittest.TestCase):
    def startUp(self):
        pass

    def test_1(self):
        pass

h = M2mHook(Revision, 'baselines')
signals.m2m_changed.connect(h.hook, weak=False, sender=Revision.baselines.through)

h = M2mHook(Collection, 'revisions')
signals.m2m_changed.connect(h.hook, weak=False, sender=Collection.revisions.through)

h = M2mHook(DocumentUpdate, 'reasons')
signals.m2m_changed.connect(h.hook, weak=False, sender=DocumentUpdate.reasons.through)

h = M2mHook(DocumentUpdate, 'assignees')
signals.m2m_changed.connect(h.hook, weak=False, sender=DocumentUpdate.assignees.through)

models_with_tags = (Event, RelativeEvent, Document, Track, Baseline, Shipment, Revision, Reason, DocumentUpdate)

for model in models_with_tags:
    h = M2mHook(model, 'tags')
    signals.m2m_changed.connect(h.hook, weak=False, sender=model.tags.through)

