from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django import forms
import forms as docman_forms
from django.db import models as django_models, IntegrityError
from django.views.decorators import http
from django.core.urlresolvers import resolve, reverse
from django.contrib import auth
from django.core import exceptions
from django.db.models.loading import get_model
from django.contrib.contenttypes.models import ContentType
from datetime import date
import tablib

from menu import Menu, SearchMenu, MenuList

import models
import utils
from search_form import SearchForm

def with_tz(request, c):
    c.update({'tz': models.TZ})


def get_menu(request):
    more_title = "more..."

    # Document Updates
    
    all_updates = Menu(title='All', url=reverse('Updates', args=('all', '/')))
    hot_updates = Menu(title='Hot', name='expired14', url=reverse('Updates', args=('expired', '/14/days')))
    current_updates = Menu(title='Current', url=reverse('Updates', args=('current', '/')))
    blocked_updates = Menu(title='Blocked', url=reverse('Updates', args=('blocked', '/')))
    done_updates = Menu(title='Done', url=reverse('Updates', args=('done', '/')))
    canceled_updates = Menu(title='Canceled', url=reverse('Updates', args=('canceled', '/')))
    archived_updates = Menu(title='Archived', url=reverse('Updates', args=('archived', '/')))

    more_updates = Menu(title=more_title, url=reverse('SearchForm', args=('DocumentUpdate',)))

    update_searches = [all_updates, blocked_updates, hot_updates, current_updates, 
            done_updates, canceled_updates, archived_updates, more_updates]

    update_menu = Menu(title='Document Updates', name='updates', url=all_updates.url, 
            subs=update_searches)

    # Documents

    doc_all = Menu(title='All', url=reverse('Documents', args=('all',)))
    doc_ext = Menu(title='External', url=reverse('Documents', args=('external',)))
    doc_int = Menu(title='Internal', url=reverse('Documents', args=('internal',)))
    more_docs = Menu(title=more_title, url=reverse('SearchForm', args=('Document',)))
    
    document_menu = Menu(title='Documents', url=doc_all.url, subs=[doc_all, doc_ext, doc_int, more_docs])

    # Revisions

    rev_all = Menu(title='All', url=reverse('Revisions', args=('all',)))
    rev_current_source = Menu(title='Current as source', name='current_source', url=reverse('Revisions', args=('current_source',)))
    rev_current_target = Menu(title='Current as target', name='current_target', url=reverse('Revisions', args=('current_target',)))
    rev_backlog = Menu(title='Backlog', url=reverse('Revisions', args=('backlog',)))
    rev_released = Menu(title='Released', url=reverse('Revisions', args=('released',)))
    more_revs = Menu(title=more_title, url=reverse('SearchForm', args=('Revision',)))

    rev_menu = Menu(title='Revisions', url=rev_all.url, subs=[rev_all, rev_current_source, rev_current_target, rev_released, more_revs])

    # Users

    users_all = Menu(title='All', url=reverse('Users', args=('all',)))
    users_with_aps = Menu(title='With APs', name='with_aps', url=reverse('Users', args=('with_aps',)))
    users_with_ups = Menu(title='With Updates', name='with_ups', url=reverse('Users', args=('with_ups',)))
    users_unassigned = Menu(title='Unassigned', url=reverse('Users', args=('unassigned',)))
    more_users = Menu(title=more_title, url=reverse('SearchForm', args=('User',)))

    user_menu = Menu(title='Users', url=users_all.url, subs=[users_all, users_with_aps, users_with_ups, users_unassigned, more_users])

    # Events

    all_events = Menu(title='All', url=reverse('RelativeEvents', args=('all', '/')))
    exp_events = Menu(title='Expired', url=reverse('RelativeEvents', args=('expired', '/')))
    week_events = Menu(title='Expired within 7 days', name='within7', url=reverse('RelativeEvents', args=('within', '/7/days')))
    week2_events = Menu(title='...14 days', name='within14', url=reverse('RelativeEvents', args=('within', '/14/days')))
    more_events = Menu(title=more_title, url=reverse('SearchForm', args=('RelativeEvent',)))

    event_menu = Menu(title='Events', name='events', url=all_events.url, subs=[all_events, exp_events, week_events, week2_events, more_events])

    # Tracks
    all_tracks = Menu(title="All", url=reverse('Tracks', args=('all',)))
    more_tracks = Menu(title=more_title, url=reverse('SearchForm', args=('Track',)))
    track_menu = Menu(title='Tracks', url=all_tracks.url, subs=(all_tracks, more_tracks,))

    # Baselines
    all_baselines = Menu(title="All", url=reverse('Baselines', args=('all',)))
    more_baselines = Menu(title=more_title, url=reverse('SearchForm', args=('Baseline',)))
    baseline_menu = Menu(title='Baselines', url=all_baselines.url, subs=(all_baselines, more_baselines,))

    # Action Points
    all_aps = Menu(title="All", url=reverse('ActionPoints', args=('all',)))
    open_aps = Menu(title="Open", url=reverse('ActionPoints', args=('open',)))
    closed_aps = Menu(title="Closed", url=reverse('ActionPoints', args=('closed',)))
    more_aps = Menu(title=more_title, url=reverse('SearchForm', args=('ActionPoint',)))
    ap_menu = Menu(title='Action Points', url=all_aps.url, subs=(all_aps, open_aps, closed_aps, more_aps))

    # Collections
    all_collections = Menu(title="All", url=reverse('Collections', args=('all',)))
    more_collections = Menu(title=more_title, url=reverse('SearchForm', args=('Collection',)))
    collection_menu = Menu(title='Collections', url=all_collections.url, subs=(all_collections, more_collections))

    # Shipments
    all_shipments = Menu(title="All", url=reverse('Shipments', args=('all',)))
    more_shipments = Menu(title=more_title, url=reverse('SearchForm', args=('Shipment',)))
    shipment_menu = Menu(title='Shipments', url=all_shipments.url, subs=(all_shipments, more_shipments))

    # Tags
    all_tags = Menu(title="All", url=reverse('Tags', args=('all',)))
    more_tags = Menu(title=more_title, url=reverse('SearchForm', args=('Tag',)))
    tag_menu = Menu(title='Tags', url=all_tags.url, subs=(all_tags, more_tags))

    # Report
    report_menu = Menu(title="Report", url=reverse('GetReport'))

    # Search
    search_menu = Menu(title='Search...', url=reverse('SearchForm'))

    return MenuList(update_menu, ap_menu, document_menu, rev_menu, user_menu, event_menu, track_menu, baseline_menu, 
            collection_menu, shipment_menu, tag_menu, report_menu, search_menu)

def get_search_menu(request):
    # Give model classes as menu names for correct activation.
    updates = Menu(title='Document Updates', name="DocumentUpdate", url=reverse('SearchForm', args=('DocumentUpdate',)))
    aps = Menu(title='Action Points', name="ActionPoint", url=reverse('SearchForm', args=('ActionPoint',)))
    docs = Menu(title='Documents', name="Document", url=reverse('SearchForm', args=('Document',)))
    revs = Menu(title='Revisions', name="Revision", url=reverse('SearchForm', args=('Revision',)))
    users = Menu(title='Users', name="User", url=reverse('SearchForm', args=('User',)))
    events = Menu(title='Events', name="Event", url=reverse('SearchForm', args=('RelativeEvent',)))
    tracks = Menu(title='Tracks', name="Track", url=reverse('SearchForm', args=('Track',)))
    baselines = Menu(title='Baselines', name="Baseline", url=reverse('SearchForm', args=('Baseline',)))
    collections = Menu(title='Collections', name="Collection", url=reverse('SearchForm', args=('Collection',)))
    shipments = Menu(title='Shipments', name="Shipment", url=reverse('SearchForm', args=('Shipment',)))
    tags = Menu(title='Tags', name="Tag", url=reverse('SearchForm', args=('Tag',)))

    return MenuList(updates, docs, aps, revs, users, events, collections, baselines, shipments, tracks, tags)

# Set by middleware when urls.py is loaded.
MENU = None
SEARCH_MENU = None

def set_context(request, c, **kwargs):
    model = kwargs.get('model')

    if model is not None:
        c['model'] = model
        c['model_name'] = model._meta.verbose_name.title()

    if request.user.is_anonymous():
        c['user'] = None
        c['user_is_cm'] = False
    else:
        c['user'] = request.user.get_profile().represent(request, shallow=True)
        p = request.user.get_profile()
        c['user_is_cm'] = models.is_user_cm(request.user)

    formatted_date, hint, warn_class = utils.format_date(date.today(), html=True)
    c['current_date'] = formatted_date

    menus = kwargs.get('menus')
    search_menus = kwargs.get('search_menus')

    utils.with_this(request, c)
    with_tz(request, c)

    if menus:
        MENU.activate(*menus)

    if search_menus:
        SEARCH_MENU.activate(*search_menus)


    c['menu'] = MENU
    c['search_menu'] = SEARCH_MENU

class CollectionSearchForm(SearchForm):
    class Meta:
        model = models.Collection
        search_by = ('name',)

class BaselineSearchForm(SearchForm):
    class Meta:
        model = models.Baseline
        search_by = ('name',)

class TagSearchForm(SearchForm):
    class Meta:
        model = models.Tag
        search_by = ('slug',)

class DocumentBasicSearchForm(SearchForm):
    class Meta:
        model = models.Document
        search_by = ('title', 'number', 'name', 'scope')

class RevisionSearchForm(SearchForm):
    class Meta:
        model = models.Revision
        search_by = (('document', DocumentBasicSearchForm, {'label': 'Document'}), 
                'number', 'tracks', ('collection', None, {'label': "In collection", 'select-widget': 'list'}),
                ('shipment', None, {'label': "In shipment", 'select-widget': 'list'}))

class UserProfileSearchForm(SearchForm):
    class Meta:
        model = auth.models.User
        search_by = ('username', 'first_name', 'last_name') 

    def queryset(self):
        user_qs = super(UserProfileSearchForm, self).queryset()

        return models.UserProfile.objects.filter(user__in=user_qs)

class RevisionBasicSearchForm(SearchForm):
    class Meta:
        model = models.Revision
        search_by = ('number',)

class DocumentSearchForm(SearchForm):
    class Meta:
        model = models.Document
        search_by = ('title', 'number', 'name', 'scope', ('tags', None, {'select-widget': 'list'}), 
                ('revision', RevisionBasicSearchForm, {'label': 'Has revision'}))

class RelativeEventSearchForm(SearchForm):
    class Meta:
        model = models.RelativeEvent
        search_by = ('title',)

class DocumentUpdateSearchForm(SearchForm):
    class Meta:
        model = models.DocumentUpdate
        search_by = ('state', 
                ('tags', None, {'select-widget': 'list'}), 
                ('from_revision__document', DocumentBasicSearchForm, {'label': 'Document'}),
                ('from_revision', RevisionBasicSearchForm, {'label': "Source revision"}),
                ('to_revision', RevisionBasicSearchForm, {'label': "Target revision"}),
                )

class TrackSearchForm(SearchForm):
    class Meta:
        model = models.Track
        search_by = ('name', )

class APSearchForm(SearchForm):
    class Meta:
        model = models.ActionPoint
        search_by = ('title', 'assignee')

class ShipmentSearchForm(SearchForm):
    class Meta:
        model = models.Shipment
        search_by = ('name', ('track__name', None, {'label': 'Track'}))

SearchModel_data = {'Document': DocumentSearchForm,
               'Revision': RevisionSearchForm,
               'User': UserProfileSearchForm,
               'DocumentUpdate': DocumentUpdateSearchForm,
               'RelativeEvent': RelativeEventSearchForm,
               'Track': TrackSearchForm,
               'ActionPoint': APSearchForm,
               'Collection': CollectionSearchForm,
               'Baseline': BaselineSearchForm,
               'Shipment': ShipmentSearchForm,
               'Tag': TagSearchForm}

SearchTemplate_data = {'User': 'userprofile'}

@http.require_GET
def search_form(request, **kwargs):
    """ Offer a model search form to user. """

    ctx = {}
    search_menus = list()

    try:
        # 'model' is default url part. 
        # So may be None if not provided.
        model = kwargs.get('model')

        if model is None:
            model = 'DocumentUpdate'

        F = SearchModel_data[model]
        ctx = {'form': F()}
        search_menus = (model,)
    except KeyError:
        raise Http404

    set_context(request, ctx, menus=('search',), search_menus=search_menus)

    return render(request, 'search_form.html', ctx)

@http.require_POST
def submit_search(request):
    query = utils.Qs.from_post(request.POST, blank=False)
    #query = utils.Qs.from_dict({'number': ['A'], 'tracks': models.Track.objects.values_list('id', flat=True)})

    search_url = query.with_url(reverse('Search'))
    return redirect(search_url)


@http.require_GET
@utils.with_qs
def search(request, query):
    try:
        model = query['searchobject'][0]
        F = SearchModel_data[model]
        form = F(query)

        object_name = SearchTemplate_data.get(model, model.lower())
        template = "%s_list.html" % object_name

        query_set = form.queryset()
        objects = models.represent_objects(request, query_set, shallow=True)

        ctx = {'list': objects, 'annotation': 'Search results:', 'no_edit_options': True}
        set_context(request, ctx)

        response = render(request, template, ctx)
    except (forms.ValidationError, KeyError), e:
        response = HttpResponse("Malformed search request.")
        #response = HttpResponse("Malformed search request: %s, form errors: %s" % (e, form.get_errors()))

    return response

#@http.require_GET
#Do not require method as it may be called either from GET or POST view.
def get_object(request, items_name, model, id, tab):
    # Nice IDs, slugs and other names are to 
    # be used in the URL, so use model's get_object() 
    # method to process the id before fetching.
    o = model.get_object(id)
    
    if o is None:
        raise Http404

    try:
        o.represent(request, tabs=True)
    except exceptions.FieldError:
        raise

    object_name = type(o).__name__.lower()
    ct = ContentType.objects.get(app_label="docman", model=object_name)

    ctx = {'model_id': ct.pk, 
           'items_name': items_name, 
           object_name: o, 
           'object': o, 
           'tab': tab}

    set_context(request, ctx, menus=(items_name,), model=model)

    return render(request, '%s.html' % object_name, ctx)

@http.require_GET
def user(request, id, tab):
    user = get_object_or_404(auth.models.User, pk=id)

    user_profile_url = reverse('UserProfile', args=(user.get_profile().pk, tab or ""))

    return redirect(user_profile_url)

@http.require_GET
def get_object_list(request, model, items_name, search_name, **kwargs):

    if not search_name:
        search_name = 'all'

    object_name = model.__name__.lower()

    template = "%s_list.html" % object_name

    objects, annotation, search_name = model.get_objects(search_name, **kwargs)

    if objects is None:
        raise Http404

    objects = objects.distinct()
    objects = models.represent_objects(request, objects, shallow=True, ordering_namespace=object_name)

    ctx = {'list': objects, 'annotation': annotation}
    set_context(request, ctx, menus=(items_name, search_name), model=model)

    return render(request, template, ctx)

@http.require_POST
def post_note(request, model_id, items_name, object_id):
    try:
        ct = ContentType.objects.get(pk=model_id)
        model = ct.model_class()
        obj = model.objects.get(pk=object_id)
    except ObjectDoesNotExist:
        raise Http404

    next_url = reverse(model.__name__, args=(obj.nice_id(), 'notes'))

    f = docman_forms.get_comment_form(request.POST)

    if not f.is_valid():
        docman_forms.PresavedErrorForm = f

        return redirect(next_url)

    note = f.cleaned_data['comment']

    obj.note(note)

    return redirect(next_url)
    

@http.require_GET
def register(request):
    ref = utils.referer(request) or ''
    form = docman_forms.RegisterForm(initial={'next_url': ref})

    return render(request, 'register.html', {'form': form})

@http.require_POST
def do_register(request):
    form = docman_forms.RegisterForm(request.POST)

    registered = False
    errors = list()

    if form.is_valid():
        password = form.cleaned_data['password']
        password2 = form.cleaned_data['password2']

        if password != password2:
            errors = ['Passwords differ']
            password_ok = False
        else:
            password_ok = True

        if password_ok:
            username = form.cleaned_data['username']
            email = form.cleaned_data['email']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']

            try:
                auth.models.User.objects.create_user(username=username, 
                    email=email, 
                    password=password,
                    first_name=first_name,
                    last_name=last_name)
            except IntegrityError:
                errors = ['Cannot register: this username might already exist.']
            else:
                registered = True

    if registered:
        return redirect(form.cleaned_data['next_url'])
    else:
        if errors:
            form.non_field_errors = errors

        return render(request, 'register.html', {'form': form})

@http.require_GET
def sign_in(request):
    ref = utils.referer(request) or ''
    form = docman_forms.SignInForm(initial={'next_url': ref})

    return render(request, 'sign_in.html', {'form': form})

@http.require_POST
def do_sign_in(request):
    form = docman_forms.SignInForm(request.POST)

    logged_in = False
    errors = list()

    if form.is_valid():
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']

        user = auth.authenticate(username=username, password=password)

        if user is None:
            errors = ["Authentication failed"]
        else:
            if user.is_active:
                auth.login(request, user)
                logged_in = True
            else:
                errors = ["Account '%s' is disabled" % user.username]

    if logged_in:
        return redirect(form.cleaned_data['next_url'])
    else:
        if errors:
            form.non_field_errors = errors

        return render(request, 'sign_in.html', {'form': form})

@http.require_GET
def sign_out(request):
    ref = utils.referer(request) or reverse('Index')

    auth.logout(request)

    return redirect(ref)

def get_report(request):
    if request.method == 'GET':
        ctx = {'form': docman_forms.ReportInputForm()}
        set_context(request, ctx, menus=('report',))

        return render(request, 'report_form.html', ctx)

    if request.method == 'POST':
        form = docman_forms.ReportInputForm(request.POST)

        if form.is_valid():
            start = form.cleaned_data['start_date']
            end = form.cleaned_data['end_date']

            return redirect(reverse('Report', args=(start, end)))
        else:
            return render(request, 'report_form.html', {'form': form})

def report_filename(start, end):
    s = utils.to_slug

    return "doc_report_%s_%s.xls" % (s(start), s(end))

def report_date(d):
    return d.strftime("%d-%b-%Y")

def get_report_data(start, end):
    t = models.Revision.get_report(start_date=start, end_date=end)

    approved = t[0]
    d = dict()

    d['approved'] = sum(map(len, approved))
    d['tr_approved'] = len(approved[0])
    d['cr_approved'] = len(approved[1])
    d['wp_approved'] = len(approved[2])
    d['other_approved'] = len(approved[3])

    inprogress = t[1]
    d['inprogress'] = sum(map(len, inprogress))
    d['tr_inprogress'] = len(inprogress[0])
    d['cr_inprogress'] = len(inprogress[1])
    d['wp_inprogress'] = len(inprogress[2])
    d['other_inprogress'] = len(inprogress[3])

    return (t, d)

@http.require_GET
def report(request, start, end):
    ctx = dict()
    set_context(request, ctx)

    ctx['start_date'] = start
    ctx['end_date'] = end

    try:
        ctx.update(get_report_data(start, end)[1])
    except ValidationError, e:
        # E. g. invalid date format or invalid date.
        raise Http404

    ctx['report_filename'] = report_filename(start, end)

    return render(request, 'report.html', ctx)

@http.require_GET
def file_report(request, kind, start, end):
    try:
        #t = models.Revision.get_report(start_date=start, end_date=end)
        fr = get_report_data(start, end)[1]
    except ValidationError, e:
        # E. g. invalid date format or invalid date.
        raise Http404

    response = None

    if kind == 'xls':
        dataset = tablib.Dataset(('', 'Total updates', 'TR', 'CR', 'WP', 'Other'))
        dataset.append(('Approved', 
            fr['approved'], 
            fr['tr_approved'],
            fr['cr_approved'],
            fr['wp_approved'],
            fr['other_approved']))

        dataset.append(('In progress', 
            fr['inprogress'], 
            fr['tr_inprogress'],
            fr['cr_inprogress'],
            fr['wp_inprogress'],
            fr['other_inprogress']))

        response = HttpResponse(dataset.xls)
        response['Content-Disposition'] = "attachment; filename=%s" % report_filename(start, end)
    
    if response is None:
        raise Http404

    return response

def index(request):
    return redirect(reverse('Updates', args=('all', '/')))

