from django import forms
from django.contrib.contenttypes.models import ContentType

PresavedErrorForm = None

def get_comment_form(*args, **kwargs):
    class CommentForm(forms.Form):
        comment = forms.CharField(widget=forms.Textarea(attrs={'cols': '80'}), max_length=1024)

        def clean_comment(self):
            text = self.cleaned_data['comment']

            trimmed_text = text.strip("\t ")

            if trimmed_text:
                self.cleaned_data['comment'] = trimmed_text

                return self.cleaned_data['comment']
            else:
                raise forms.ValidationError("Empty comment")

    global PresavedErrorForm

    if (not args) and PresavedErrorForm:
        return PresavedErrorForm
    else:
        return CommentForm(*args, **kwargs)

class SignInForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    next_url = forms.CharField(widget=forms.HiddenInput)

class RegisterForm(forms.Form):
    username = forms.CharField()
    first_name = forms.CharField()
    last_name = forms.CharField()
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)
    password2 = forms.CharField(widget=forms.PasswordInput, label="Repeat password")

    next_url = forms.CharField(widget=forms.HiddenInput)


class ReportInputForm(forms.Form):
    w = forms.DateInput(format="%d-%b-%Y")

    # Input formats: 01-Jan-2013, 01-01-2013
    in_fmt=("%d-%b-%Y", "%d-%m-%Y")
    #in_fmt_str=", ".join(in_fmt)

    help = "E. g. 01-Jan-2013 or 01-01-2013."

    start_date = forms.DateField(widget=w, input_formats=in_fmt, help_text=help)
    end_date = forms.DateField(widget=w, input_formats=in_fmt)
