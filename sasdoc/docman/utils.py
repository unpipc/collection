import urlparse
import urllib
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.template import defaultfilters
from django.utils.safestring import mark_safe
from datetime import date

def get_admin_url(obj, action='change'):
    ct = ContentType.objects.get_for_model(obj.__class__)

    r = None

    if action == 'change' and not obj.unsaved():
        r = reverse("admin:%s_%s_%s" % (ct.app_label, ct.model, action), args=(obj.pk,))

    if action == 'add':
        r = reverse("admin:%s_%s_%s" % (ct.app_label, ct.model, action))

    return r

def referer(request):
        return request.META.get('HTTP_REFERER', None)

def with_back(request, c):
        c.update({'back': referer(request)})

def with_this(request, c):
        c.update({'this': request.path_info})

def isiterable(obj):
    try:
        iter(obj)
        return True
    except TypeError:
        return False

# Start string from capital.
def sentence(s):
    return s[0].upper() + s[1:]

def unique_only(l, comparator):
    i = 0

    while True:
        length = len(l)

        if i >= length:
            break

        j = i + 1

        while j < length:
            if comparator(l[j], l[i]):
                l.pop(j)
                length -= 1
            else:
                j += 1

        i += 1

    return l

def unique_join(*lists, **kwargs):
    default_comparator = lambda a, b: a == b
    comparator = kwargs.get('comparator', default_comparator)

    all = list()

    for l in lists:
        if isinstance(l, list):
            all += l

        if isinstance(l, tuple):
            all += list(l)

    return unique_only(all, comparator)


class Qs(dict):
        def __init__(self, request=None, **kwargs):
            super(Qs, self).__init__()

            if request:
                qs = self.__class__.query_string(request)
                self.update(qs)
                self.remove_blanks(**kwargs)

        @classmethod
        def query_string(cls, request):
                qs = request.META.get('QUERY_STRING', None)

                return urlparse.parse_qs(qs)

        def remove_blanks(self, **kwargs):
            blank = kwargs.get('blank', True)

            if not blank:
                for k, vlist in self.items():
                    no_blanks = filter(lambda v: v != '', vlist)
                    no_blanks or self.pop(k)

        def to_ctx(self, ctx, *keys):
                for key in keys:
                    ctx[key] = self.get(key)

        @classmethod
        def from_dict(cls, d, **kwargs):
            qs = cls()
            qs.update(d)
            qs.remove_blanks(**kwargs)

            return qs

        @classmethod
        def from_post(cls, post, **kwargs):
            d = dict()

            for key, value in post.iterlists():
                d[key] = value

            d.pop('csrfmiddlewaretoken')

            return Qs.from_dict(d, **kwargs)

        def with_url(self, url):
            return "%s?%s" % (url,  urllib.urlencode(self.items(), True))

        def get_subquery(self, namespace):
            keys = filter(lambda k: k.startswith("%s:" % namespace), self.keys())

            sub = dict()
            for k in keys:
                sub_k = k.split(":")[1]
                sub[sub_k] = self[k]

            return sub

        def get_first(self, key, default=None):
            return self.__get_by_index(key, 0, default)

        def get_last(self, key, default=None):
            return self.__get_by_index(key, -1, default)

        def __get_by_index(self, key, idx, default=None):
            v = self.get(key)

            if v is None:
                return default
            else:
                return v[idx]


def with_qs(f):
        def tmp(request, *args, **kw):
                return f(request, Qs(request), *args, **kw)

        return tmp


def qs_sub(qs1, qs2):
    return qs1.exclude(pk__in=qs2.values_list('pk', flat=True))

def obj_dump(obj, **kwargs):
    verbose = kwargs.get('verbose', False)
    no_fields = kwargs.get('no_fields', list())

    dump = list()

    for f in obj._meta.fields:
        name = f.name
        verbose_name = unicode(f.verbose_name)

        if name == 'digest':
            continue

        if name in no_fields:
            continue

        v = getattr(obj, name)

        if verbose:
            dump.append("%s: %s" % (verbose_name, unicode(v)))
        else:
            dump.append(unicode(v))

    if verbose:
        r = ", ".join(dump)
    else:
        r = "".join(dump)

    return r

def to_slug(s, **kwargs):
    upper = kwargs.get('upper', False)

    if upper:
        s = s.upper()
    else:
        s = s.lower()

    import re

    r = re.compile("[^a-zA-Z0-9_-]+")

    return r.sub("_", s).strip("_")
    
class MalformedUrlException(Exception):
    def __init__(self, url):
        super(MalformedUrlException, self).__init__("Malformed url: %s" % url)

def parse_url(url, delim, *type_pattern_tuple):
    import re

    parsed = list()

    chunks = filter(lambda x: x != '', url.split(delim))
    l = min(len(chunks), len(type_pattern_tuple))

    for i in range(0, l):
        chunk = chunks[i]

        t = type_pattern_tuple[i]
        cls = t[0] or str
        pattern = t[1]

        r = re.compile(pattern)

        if not r.match(chunk):
            raise MalformedUrlException(url)

        try:
            if cls == int:
                chunk = float(chunk)

            v = cls(chunk)
            parsed.append(v)
        except ValueError:
            raise MalformedUrlException(url)

    return parsed

def days_phrase(n, if_zero="0 days"):
    a = abs(n)

    if a > 1:
        return "%d days" % n
    else:
        if a == 1:
            return "1 day"
        else:
            return if_zero

def dashed_underline(html):
    return "<span class='dashed-underline'>%s</span>" % html

def format_date(user_date, **kwargs):
    html = kwargs.get('html', False)
    text = kwargs.get('text', None)
    class_expired = kwargs.get('class_expired', 'expired')
    class_due = kwargs.get('class_due', 'due')
    warn_expiration = kwargs.get('warn_expiration', False)
    hint_fmt = kwargs.get('hint_fmt', "%s")

    yy = user_date.strftime('%y')
    yyyy, week_number, week_day = user_date.isocalendar()

    if text is None:
        if html:
            fmt = '%s-%%b-%%d&nbsp;&mdash;&nbsp;W%s%.2d-%d' % (yy, yy, week_number, week_day)
        else:
            fmt = '%s-%%b-%%d - W%s%.2d-%d' % (yy, yy, week_number, week_day)

        text = user_date.strftime(fmt)

    if warn_expiration:
        today = date.today()
        delta = user_date - today
        ndays = abs(delta.days)

        if user_date < today:
            title = "Expired %s ago." % days_phrase(ndays)
            klass = class_expired
        elif user_date == today:
            title = "Expires today."
            klass = class_due
        else:
            title = "Will be expired in %s." % days_phrase(ndays)
            klass = class_due

        if html:
            hint = hint_fmt % title

            text = dashed_underline(text)
            text = "<span title='%(title)s' class='%(class)s'>%(date)s</span>" % \
                    {'title': hint, 'class': klass, 'date': text}


    if html:
        text = mark_safe(text)

    if warn_expiration:
        return (text, hint, klass)
    else:
        return (text, None, None)

def pack_text(text, **kwargs):
    hint = kwargs.get('hint')
    limitchars = kwargs.get('limitchars')
    html = kwargs.get('html', False)
    hint_on_overflow = kwargs.get('hint_on_overflow', False)

    visible_text = text
    overflow = False
    l = len(text)

    if limitchars and l > limitchars:
        # Let trailing part be shorter than start one.
        trailing = (limitchars - 3)/ 2

        start = limitchars - 3 - trailing

        visible_text = "%s...%s" % (text[:start], text[l-trailing:])
        overflow = True

    html_text = visible_text

    if hint_on_overflow and not overflow:
        pass
    elif hint is not None:
        html_text = mark_safe("<span title='%s'>%s</span>" % 
                (defaultfilters.escape(hint), visible_text))

    return html_text
