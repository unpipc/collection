function init_tab(list_id, tab_id) {
    target_list = document.getElementById(list_id) 

    target_li = undefined

    if (tab_id) {
        children = target_list.getElementsByTagName('li')

        for (var i = 0; i < children.length; i++) {
            li = children[i]

            if (li.getAttribute('content_id') == tab_id) {
                target_li = li
                break
            }
        }
    }

    if (target_li == undefined) {
        target_li = target_list.getElementsByTagName('li')[0]
    }

    _in_tab(target_li)
}

function in_tab_event(event) {
    target_li = event.target || event.srcElement

    _in_tab(target_li)
}

function _in_tab(target_li) {
    r = false

    try {
        target_list = target_li.parentElement
    } catch (err) {
        return r
    }

    content_id = target_li.getAttribute('content_id')

    container = document.getElementById('object-container')
    children = container.getElementsByTagName('div')

    for (var i = 0; i < children.length; i++) {
        content = children[i]

        if (content.id == content_id) { 
            content.className = "visible"
            r = true
        } else {
            content.className = "hidden"
        }
    }

    children = target_list.getElementsByTagName('li')

    for (var i = 0; i < children.length; i++) {
        li = children[i]

        if (li == target_li) { 
            li.className = "tab-visible"
        } else {
            li.className = "tab-hidden"
        }
    }
}
