from django.conf.urls import patterns, include, url
from django.contrib import admin
from docman import views
from docman import models

admin.autodiscover()

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()


def docman_object_view(items_name, model):
    def docman_wrapper(request, id, tab):
        return views.get_object(request, items_name, model, id, tab)

    return docman_wrapper

def docman_object_list_view(items_name, model):
    def docman_wrapper(request, search_name='', **kwargs):
        return views.get_object_list(request, model, items_name, search_name, **kwargs)

    return docman_wrapper

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'sasdoc.views.home', name='home'),
    # url(r'^sasdoc/', include('sasdoc.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    #url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    url(r'^search/(?P<model>.+)?$', views.search_form, name='SearchForm'),
    url(r'^search$', views.search, name='Search'),
    url(r'^submit-search$', views.submit_search, name='SubmitSearch'),

    url(r'^$', views.index, name='Index'),

    url(r'^updates/(?P<search_name>[^/]+)?(?P<search_attributes>.*)$', docman_object_list_view('updates', models.DocumentUpdate), name='Updates'),
    url(r'^documents/(?P<search_name>.+)?$', docman_object_list_view('documents', models.Document), name='Documents'),
    url(r'^revisions/(?P<search_name>.+)?$', docman_object_list_view('revisions', models.Revision), name='Revisions'),
    url(r'^users/(?P<search_name>.+)?$', docman_object_list_view('users', models.UserProfile), name='Users'),
    url(r'^relative_events/(?P<search_name>[^/]+)?(?P<search_attributes>.*)$', docman_object_list_view('events', models.RelativeEvent), name='RelativeEvents'),
    url(r'^tracks/(?P<search_name>[^/]+)?$', docman_object_list_view('tracks', models.Track), name='Tracks'),
    url(r'^baselines/(?P<search_name>[^/]+)?$', docman_object_list_view('baselines', models.Baseline), name='Baselines'),
    url(r'^action_points/(?P<search_name>[^/]+)?$', docman_object_list_view('action_points', models.ActionPoint), name='ActionPoints'),
    url(r'^tags/(?P<search_name>[^/]+)?$', docman_object_list_view('tags', models.Tag), name='Tags'),
    url(r'^collections/(?P<search_name>[^/]+)?$', docman_object_list_view('collections', models.Collection), name='Collections'),
    url(r'^shipments/(?P<search_name>[^/]+)?$', docman_object_list_view('shipments', models.Shipment), name='Shipments'),

    url(r'^document/(?P<id>.+)/(?P<tab>.+)?$', docman_object_view('documents', models.Document), name='Document'),
    url(r'^revision/(?P<id>.+)/(?P<tab>.+)?$', docman_object_view('revisions', models.Revision), name='Revision'),
    url(r'^update/(?P<id>.+)/(?P<tab>.+)?$', docman_object_view('updates', models.DocumentUpdate), name='DocumentUpdate'),
    url(r'^track/(?P<id>.+)/(?P<tab>.+)?$', docman_object_view('tracks', models.Track), name='Track'),
    url(r'^baseline/(?P<id>.+)/(?P<tab>.+)?$', docman_object_view('baselines', models.Baseline), name='Baseline'),
    url(r'^action_point/(?P<id>.+)/(?P<tab>.+)?$', docman_object_view('action_points', models.ActionPoint), name='ActionPoint'),
    url(r'^reason/(?P<id>.+)/(?P<tab>.+)?$', docman_object_view('reasons', models.Reason), name='Reason'),
    url(r'^event/(?P<id>.+)/(?P<tab>.+)?$', docman_object_view('events', models.Event), name='Event'),
    url(r'^relative_event/(?P<id>.+)/(?P<tab>.+)?$', docman_object_view('events', models.RelativeEvent), name='RelativeEvent'),
    url(r'^tag/(?P<id>.+)/(?P<tab>.+)?$', docman_object_view('tags', models.Tag), name='Tag'),
    url(r'^collection/(?P<id>.+)/(?P<tab>.+)?$', docman_object_view('collections', models.Collection), name='Collection'),
    url(r'^shipment/(?P<id>.+)/(?P<tab>.+)?$', docman_object_view('shipments', models.Shipment), name='Shipment'),
    url(r'^user_profile/(?P<id>.+)/(?P<tab>.+)?$', docman_object_view('users', models.UserProfile), name='UserProfile'),

    url(r'^get-report$', views.get_report, name='GetReport'),
    url(r'^report/(?P<start>.+)/(?P<end>.+)$', views.report, name='Report'),
    url(r'^file-report/(?P<kind>.+)/(?P<start>.+)/(?P<end>.+)$', views.file_report, name='FileReport'),

    url(r'^post-comment/(?P<model_id>\d+)/(?P<items_name>.+)/(?P<object_id>\d+)$', views.post_note, name='PostNote'),

    url(r'^register$', views.register, name='Register'),
    url(r'^do-register$', views.do_register, name='DoRegister'),

    url(r'^sign-in$', views.sign_in, name='SignIn'),
    url(r'^do-sign-in$', views.do_sign_in, name='DoSignIn'),
    url(r'^sign-out$', views.sign_out, name='SignOut')
)

# User solution from:
# http://stackoverflow.com/questions/11994004/django-project-with-gunicorn-server-on-heroku-does-not-serve-static-files
# or merge with 'production' branch.
